// flow-typed signature: 54a02c70c63eff7b01e51317d7e20850
// flow-typed version: <<STUB>>/dat-gui_v^0.5.0/flow_v0.47.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'dat-gui'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'dat-gui' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'dat-gui/vendor/dat.color' {
  declare module.exports: any;
}

declare module 'dat-gui/vendor/dat.gui' {
  declare module.exports: any;
}

// Filename aliases
declare module 'dat-gui/index' {
  declare module.exports: $Exports<'dat-gui'>;
}
declare module 'dat-gui/index.js' {
  declare module.exports: $Exports<'dat-gui'>;
}
declare module 'dat-gui/vendor/dat.color.js' {
  declare module.exports: $Exports<'dat-gui/vendor/dat.color'>;
}
declare module 'dat-gui/vendor/dat.gui.js' {
  declare module.exports: $Exports<'dat-gui/vendor/dat.gui'>;
}
