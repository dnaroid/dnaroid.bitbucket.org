/* eslint-disable no-process-env */
/* global require, __dirname, module, process */

const path              = require('path')
const webpack           = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const BabiliPlugin      = require('babili-webpack-plugin')

const nodeEnv      = process.env.NODE_ENV || 'development'
const nodeFlow     = process.env.NODE_FLOW || 'enabled'
const isProduction = nodeEnv === 'production'
const isFlow       = nodeFlow === 'enable'

const preprocessorOptions = {
  DEBUG: !isProduction,
  LOG  : !isProduction,
}
const preprocessorQuery   = require('querystring').encode({ json: JSON.stringify(preprocessorOptions) })

console.log(`--- ${nodeEnv} mode ---`)

if (!isProduction) {
  console.log(`--- flow-runtime ${nodeFlow} ---`)
}

const devPath         = path.join(__dirname, '/dev')
const buildPath       = path.join(__dirname, '/build')
const nodeModulesPath = path.join(__dirname, '/node_modules')
const sourcePath      = path.join(__dirname, '/src')

const outputPath = isProduction ? buildPath : devPath

let plugins = [
  new webpack.NoEmitOnErrorsPlugin(),

  new HtmlWebpackPlugin({
    template: path.join(sourcePath, 'index.html'),
    filename: path.join(outputPath, 'index.html'),
    favicon : 'favicon.ico',
  }),
]

const jsDevLoader = {
  loader : 'babel-loader',
  options: {
    presets: ['flow', 'stage-0'],
    plugins: isFlow ? [
      ['flow-runtime', { assert: true, annotate: true }],
      'transform-decorators-legacy',
    ] : [
      'transform-decorators-legacy',
    ],
  },
}

const jsProdLoader = {
  loader : 'babel-loader',
  options: {
    presets: ['babili', 'react', 'stage-0'],
    plugins: ['transform-decorators-legacy'],
  },
}

const jsMetaRule = {
  test   : /\.js$/,
  exclude: /node_modules/,
  use    : path.join(__dirname, `/src/app/lib/webpackLoaderPreprocessor/preprocessor-loader?${preprocessorQuery}`),
}
const jsRule     = isProduction ? {
  test: /\.js$/,
  use : jsProdLoader,
} : {
  test   : /\.js$/,
  exclude: nodeModulesPath,
  use    : jsDevLoader,
}

const cssRule = {
  test: /\.css$/,
  use : ['style-loader', 'css-loader'],
}

if (isProduction) {
  plugins = [
    ...plugins,

    new webpack.LoaderOptionsPlugin({ minimize: true, debug: false }),

    new BabiliPlugin(),

  ]
} else {
  plugins = [
    ...plugins,
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
  ]
}

module.exports = {
  entry: {
    app: 'App.js',
  },

  output: {
    path             : outputPath,
    filename         : isProduction ? 'app-[hash].js' : 'app.js',
    sourceMapFilename: 'app.map',
  },

  module: {
    rules: [
      jsRule,
      jsMetaRule,
      cssRule,
    ],
  },

  stats: { colors: true },

  resolve: {
    extensions: ['.webpack-loader.js', '.web-loader.js', '.loader.js', '.js'],
    modules   : [sourcePath, nodeModulesPath],
  },

  plugins,

  devtool: isProduction ? 'nosources-source-map' : 'source-map',

  context: sourcePath,

  watch: false,

  devServer: {
    historyApiFallback: true,
    port              : 8080,
    compress          : isProduction,
    inline            : !isProduction,
    hot               : !isProduction,
    host              : '0.0.0.0',

    watchOptions: {
      watch: false,
    },

    stats: {
      assets    : true,
      children  : false,
      chunks    : false,
      hash      : false,
      modules   : false,
      publicPath: false,
      timings   : true,
      version   : false,
      warnings  : true,
      colors    : { green: '\u001b[32m' },
    },
  },
}
