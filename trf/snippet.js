/*
 https://www.heroforge.com/

 in creationkit.js set bp:

 return delete this.geometry

 downloadSTL(r)

 */

THREE.STLExporter = function () { }
THREE.STLExporter.prototype = {
  constructor: THREE.STLExporter,
  parse: (function () {
    var vector = new THREE.Vector3();
    var normalMatrixWorld = new THREE.Matrix3();
    return function parse(object) {
      var output = '';
      output += 'solid exported\n';
      if (object instanceof THREE.Mesh) {
        var geometry = object.geometry;
        var matrixWorld = object.matrixWorld;
        if (geometry instanceof THREE.BufferGeometry) {
          geometry = new THREE.Geometry().fromBufferGeometry(geometry);
        }
        if (geometry instanceof THREE.Geometry) {
          var vertices = geometry.vertices;
          var faces = geometry.faces;
          normalMatrixWorld.getNormalMatrix(matrixWorld);
          for (var i = 0, l = faces.length; i < l; i++) {
            var face = faces[i];
            vector.copy(face.normal).applyMatrix3(normalMatrixWorld).normalize();
            output += '\tfacet normal ' + vector.x + ' ' + vector.y + ' ' + vector.z + '\n';
            output += '\t\touter loop\n';
            var indices = [face.a, face.b, face.c];
            for (var j = 0; j < 3; j++) {
              vector.copy(vertices[indices[j]]).applyMatrix4(matrixWorld);
              output += '\t\t\tvertex ' + vector.x + ' ' + vector.y + ' ' + vector.z + '\n';
            }
            output += '\t\tendloop\n';
            output += '\tendfacet\n';
          }
        }
      }
      output += 'endsolid exported\n';
      return output;
    }
  }())
};

stlexporter = new THREE.STLExporter()

downloadSTL = function (geometry) {

  var material = new THREE.MeshPhongMaterial({
    color: 0xF5F5F5
  })

  var model = new THREE.Mesh(geometry, material)

  var data = stlexporter.parse(model)
  var a = window.document.createElement('a')
  a.href = window.URL.createObjectURL(new Blob([data], {
    type: 'text/csv'
  }))
  a.download = geometry.name + '.stl'
  document.body.appendChild(a)
  a.clickBoard()
  document.body.removeChild(a)
}
