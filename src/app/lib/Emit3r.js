// @flow
type Waiter = {
  args: any,
  methodName: string,
}

type Collector = {
  complete: Object,
  fn: Function,
  argsByName: Object,
  caller: string,
}

const waitersNewInstance: WeakMap<Object, Array<Waiter>> = new WeakMap()
let eventCollectors: Object                              = {}
let eventsHistory: Object                                = {}
let historyLink                                          = []
let mailBox: Object                                      = {}
let spyIncludeFilter: Array<string> | string             = []
let spyExcludeFilter: Array<string>                      = []
let subscribers: Object                                  = {}
let spyMode: boolean                                     = false
let logEmitMode: boolean                                 = false
let EVENTS_HISTORY_SIZE: number                          = 1000
let debugMode: boolean                                   = false

let STORE_UPDATE_PREFIX = 'CHANGE'

const SPY_NAME: string = '[SPY]'

function log(...args: any) {
  if (debugMode) { console.debug(...args) }
}

// TODO: fix for safari
function getCaller(level: number = 0): string {
  let arr = []
  try { throw new Error() }
  catch (e) {
    arr = e.stack
      .replace(/\(.*\)/g, '')
      .replace(/http.*\/(.*\.js):.*/g, '$1')
      .replace('new ', '')
      .split(/\s+at\s/)
      .filter(s => (
        s !== 'Error'
        && s !== 'getCaller'
        && s !== 'when'
        && s !== 'popMail'
        && s !== 'emit'
        && !s.startsWith('Array.')
      ))
  }
  return arr[level].trim()
}

function updateHistory() {
  while (historyLink.length > EVENTS_HISTORY_SIZE) {
    const [lastEventName] = historyLink.splice(0, 1)
    eventsHistory[lastEventName].splice(0, 1)
  }
}

function addEventToHistory(name: string, ...args) {
  if (eventsHistory[name] === undefined) { eventsHistory[name] = [] }
  const event = { args }
  eventsHistory[name].push(event)
  historyLink.push(name)
  if (historyLink.length > EVENTS_HISTORY_SIZE) {
    updateHistory()
  }
}

function emitFromHistory(name: string, fn: Function) {
  const argsArr = eventsHistory[name]

  if (argsArr) {
    argsArr.forEach(({ args }) => {
      // #if DEBUG
      if (logEmitMode) {
        console.debug(`[defered] ->`, name, args)
      }
      // #endif
      fn.apply(null, args)
    })
  }
}

function isMatchSpyFilter(name: string) {
  name = name.toLowerCase()
  for (let i = 0; i < spyExcludeFilter.length; i++) {
    if (name.includes(spyExcludeFilter[i])) {return false}
  }
  if (spyIncludeFilter === '*') { return true }
  for (let i = 0; i < spyIncludeFilter.length; i++) {
    if (name.includes(spyIncludeFilter[i])) { return true }
  }
  return false
}

function subscribeSpy(name: string) {
  if (isMatchSpyFilter(name)) {
    let spyIndex: number         = -1
    const callers: Array<string> = []

    subscribers[name]
      .forEach((fn, idx) => {
        const caller: string = fn.__subscriber

        if (caller === SPY_NAME) {
          spyIndex = idx
        } else {
          callers.push(caller)
        }
      })

    const spyFn: Function = (...args) =>
      console.debug(name, args, '->', callers.join(', '))

    spyFn.__subscriber = SPY_NAME

    if (spyIndex >= 0) {
      subscribers[name].splice(spyIndex, 1)
    }
    subscribers[name] = [spyFn, ...subscribers[name]]
  }
}

function subscribeCollector(names: Array<string>, fn: Function, caller: string) {
  const argsByName = {}
  const complete   = {}

  fn.__subscriber = caller
  names.forEach(name => complete[name] = false)
  const waiter = { fn, complete, caller, argsByName }

  names.forEach(name => {
    if (eventCollectors[name] === undefined) { eventCollectors[name] = [] }
    eventCollectors[name].push(waiter)
  })
}

function emitPart(waiter: Collector, name: string, args: any) {
  const { complete, fn, argsByName } = waiter

  argsByName[name] = args
  complete[name]   = true
  // #if DEBUG
  if (logEmitMode) {
    console.debug('[partial] ->', name, args)
  }
  // #endif
  if (Object.values(complete).every(value => value === true)) {
    fn.apply(this, [argsByName])
    Object.keys(complete).forEach(n => complete[n] = false)
  }
}

function emitToCollector(name: string, args: any) {
  const collectors = eventCollectors[name]

  collectors.forEach(waiter => emitPart(waiter, name, args))
}

function subscribe(name: string, fn: Function, caller: string) {
  fn.__subscriber = caller

  if (subscribers[name] === undefined) { subscribers[name] = [] }
  subscribers[name].push(fn)

  if (spyMode) {
    subscribeSpy(name)
  }
  log(`${caller} subscribed to ${name}`)
  emitFromHistory(name, fn)
}

function subscribeOnDOMEvent(node: Object, eventName: string, fn: Function, caller: string) {
  const newEventName: string = `${node.id || node.constructor.name}_${eventName}`

  fn.__DOMEvent   = true
  fn.__subscriber = caller

  if (subscribers[newEventName] === undefined) { subscribers[newEventName] = [] }
  subscribers[newEventName].push(fn)
  node.addEventListener(eventName, fn, false)
  log(`${caller} subscribed to DOM event ${eventName}`)
}

function addToInstanceWaiters(prototype: Object, methodName: string, args: any) {
  const waiters        = waitersNewInstance.get(prototype) || []
  const waiter: Waiter = { args, methodName }
  waiters.push(waiter)
  waitersNewInstance.set(prototype, waiters)
}

function subscribeWaiters(instance: Object) {
  const prototype: Object       = Object.getPrototypeOf(instance)
  const className: string       = instance.constructor.name
  const waiters: ?Array<Waiter> = waitersNewInstance.get(prototype)

  if (waiters) {
    waiters.forEach(waiter => {
      const { args, methodName } = waiter
      const fn                   = prototype[methodName]
      const method: Function     = (...arg) => fn.apply(instance, arg)

      if (args === undefined) { return }
      for (let i = 0; i < args.length; i++) {
        const arg = args[i]
        if (Array.isArray(arg)) {
          subscribeCollector(arg, method, className)
        } else if (typeof arg === 'object') {
          subscribeOnDOMEvent(arg, args[i + 1], method, className)
          i++
        } else {
          subscribe(arg, method, className)
        }
      }
    })
  }
}

export function __getSubscribers(): Object { return subscribers}

export function __getEventCollectors(): Object { return eventCollectors}

export function __getEventsHistory(): Object { return eventsHistory }

export function __getMailBox(): Object { return mailBox }

/* ----------------------------- API ----------------------------- */

export function logAllEmits() { logEmitMode = true}

export function setEventsHistorySize(size: number) {
  EVENTS_HISTORY_SIZE = size
  if (size === 0) {
    eventsHistory = {}
    historyLink   = []
  } else {
    updateHistory()
  }
}

/***
 *
 * @description Set filter for log events. Sample: spy('click|~hover|move')
 *
 * @param {?string} matchStr - matchers separated by '|'. Char '~' for ignore log.
 * @returns {void}
 */
export function spy(matchStr: string = '*') {
  const filter = matchStr ? matchStr.toLowerCase().split('|') : null

  spyIncludeFilter = []
  spyExcludeFilter = []

  if (filter) {
    spyMode = true
    filter.forEach(str => {
      if (str.startsWith('~')) {
        spyExcludeFilter.push(str.replace('~', ''))
      } else {
        //$FlowFixMe
        spyIncludeFilter.push(str)
      }
    })
    if (spyIncludeFilter.length === 0) {
      spyIncludeFilter = '*'
    }
  }
}

/***
 *
 * @description Subscribe on events. Also Subscribe on DOM events.
 *
 * @param {*} args - sample:
 *
 * @example  when( EVENT1, EVENT2, callback1,
 *                 [EVENT3, EVENT4] , callback2,
 *                 document, 'click', callback3 )
 *
 * @returns {void}
 */
export function when(...args: any) {
  const caller: string = getCaller()
  let params           = []

  args.forEach((arg, idx) => {
    if (typeof arg === 'function') {
      if (typeof args[idx - 2] === 'object' && typeof args[idx - 1] === 'string') {
        subscribeOnDOMEvent(args[idx - 2], args[idx - 1], arg, caller)
      } else {
        params.forEach(param => {
          if (Array.isArray(param)) {
            subscribeCollector(param, arg, caller)
          } else {
            subscribe(param, arg, caller)
          }
        })
        params = []
      }
    } else {
      params.push(arg)
    }
  })
}

/***
 *
 * @description Send event
 *
 * @param {string} name - event name
 * @param {...*} args - any args
 * @returns {void}
 */
export function emit(name: string, ...args: any) {
  const callbacks: ?Array<Function> = subscribers[name]

  if (callbacks) {
    callbacks.forEach(fn => fn.apply(this, args))
  }
  if (eventCollectors[name]) {
    emitToCollector(name, args)
  }
  if (EVENTS_HISTORY_SIZE > 0) {
    addEventToHistory(name, ...args)
  }
  // #if DEBUG
  if (logEmitMode) { console.debug(getCaller(), '->', name, args) }
  // #endif
}

/***
 *
 * @description Push on-demand event(s)
 *
 * @param {string} to - subscriber
 * @param {Object | Array} events - events object {event: EVENT1, args: [arg1,arg2]}
 * @returns {void}
 */
export function pushMail(to: number | string, events: Object | Array<Object>) {
  if (mailBox[to] === undefined) {
    mailBox[to] = []
  }
  mailBox[to].push(events)
}

/***
 *
 * @description Get next on-demand event
 *
 * @param {string} to - subscriber
 * @returns {void}
 */
export function popMail(to: number | string) {
  const mail = mailBox[to]

  if (mail && mail.length > 0) {
    const [events] = mail.splice(0, 1)
    if (Array.isArray(events)) {
      events.forEach(({ event, args }) => emit(event, ...args))
    } else {
      emit(events.event, ...events.args)
    }
  }
}

/***
 * @description Clear on demand events
 *
 * @param {string} name - subscriber
 * @returns {void}
 */
export function clearMail(name: number | string) {
  mailBox[name] = []
}

/***
 *
 * @description Clear all
 *
 * @returns {void}
 */
export function reset() {
  historyLink      = []
  subscribers      = {}
  eventCollectors  = {}
  mailBox          = {}
  spyIncludeFilter = []
  eventsHistory    = {}
  spyExcludeFilter = []
  debugMode        = false
  spyMode          = false
  logEmitMode      = false
}

/***
 *
 * @description Enable global logging. Context available as global object '$$'
 *
 * @returns {void}
 */
export function enableDebug() {
  debugMode = true
  window.$$ = {
    subscribers,
    eventsHistory,
    mailBox,
    waitersNewInstance,

    reset,
    spy,
    when,
    emit,
    clearMail,
    popMail,
    pushMail,
  }
}

export function setStoreUpdatePrefix(prefix: string) {
  STORE_UPDATE_PREFIX = prefix
}

// class decorator neccesary for use @on() decorator.
export function listen(target: Function): any {
  const handler    = {
    construct: function (targ, args) {
      const instance = Reflect.construct(targ, args)
      subscribeWaiters(instance)
      return instance
    },
  }
  const descriptor = Object.getOwnPropertyDescriptor(target.prototype, 'constructor')
  Object.defineProperty(target, 'constructor', descriptor)
  return new Proxy(target, handler)
}

/***
 *
 * @description Method decorator for subscribe method on event(s).
 *               Subscribe on events. Also Subscribe on DOM events.
 *
 * @param {*} args - events ids or Dom events
 *
 * @example   @On( EVENT1, EVENT2,
 *                 [EVENT3, EVENT4],   // collect events
 *                 document, 'click' )
 *            method(eventData){
 *              .....
 *            }
 *
 * @returns {void}
 */
export function on(...args: any): any {
  return function (target: Object, methodName: string, descriptor: Object) {
    addToInstanceWaiters(target, methodName, args)
    return descriptor
  }
}

/***
 *
 * @description Subscribe on object updates.
 *
 * @param {Object} object - object for subscribe
 * @param {?string} alias - event alias name
 * @returns {Proxy} - proxy
 */
export function watch(object: Object, alias?: string): Object {
  const aliasName = alias !== undefined ? `.${alias}` : ''

  const handler = {
    set: function (obj, prop, value) {
      obj[prop] = value
      // #if DEBUG
      console.log(`${STORE_UPDATE_PREFIX}${aliasName}.${prop}`, value)
      // #endif
      emit(`${STORE_UPDATE_PREFIX}${aliasName}.${prop}`, value)
      return true
    },
  }
  return new Proxy(object, handler)
}

