const IF_RX   = /^[\s]*\/\/[\s]*#if(.+)$/g
const ELSE_RX = /^[\s]*\/\/[\s]*#else.*$/g
const END_RX  = /^[\s]*\/\/[\s]*#endif.*$/g

const OUTSIDE     = 0
const INSIDE_THEN = 1
const INSIDE_ELSE = 2

function evaluate(cond, varsString) {
// eslint-disable-next-line no-new-func
  return new Function(`{${varsString};return (${cond});}`)()
}

function checkOutside(state) {
  if (state !== OUTSIDE) {
    throw new Error('#endif not found')
  }
}

function parse(source, vars) {
  const varsString = Object.keys(vars).map(key => `var ${key}=${vars[key]}`).join(';')
  const lines      = source.split('\n')
  const newLines   = []
  const len        = lines.length
  let isTrue       = false
  let state        = OUTSIDE

  for (let i = 0; i < len; i++) {
    const line      = lines[i]
    const matchIf   = IF_RX.exec(line)
    const matchElse = ELSE_RX.exec(line)
    const matchEnd  = END_RX.exec(line)
    if (matchIf) {
      checkOutside(state)
      isTrue = !!evaluate(matchIf[1].trim(), varsString)
      state  = INSIDE_THEN
    } else if (matchElse) {
      state = INSIDE_ELSE
    } else if (matchEnd) {
      state = OUTSIDE
    } else if (state === OUTSIDE ||
               (state === INSIDE_THEN && isTrue) ||
               (state === INSIDE_ELSE && !isTrue)) {
      newLines.push(line)
    }
  }
  checkOutside(state)
  source = newLines.join('\n')
  return source
}

// eslint-disable-next-line no-undef
exports.parse = parse