const url          = require('url')
const queryString  = require('querystring')
const preprocessor = require('./preprocessor')

module.exports = function (source, map) {
  this.cacheable && this.cacheable()
  const query   = queryString.parse(url.parse(this.query).query)
  const options = JSON.parse(query.json)
  try {
    source = preprocessor.parse(source, options)
    this.callback(null, source, map)
  }
  catch (err) {
    const errorMessage = 'preprocessor error: ' + err
    this.callback(new Error(errorMessage))
  }
}
