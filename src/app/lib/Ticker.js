// @flow
let tickers = []

export default class Ticker {
  bk: Object
  fn: Function
  obj: Object
  time: number
  keys: Array<string>
  target: Object
  deltas: Object
  repeats: number
  counter: number
  started: boolean
  timeStep: number
  yoyoMode: boolean
  alwaysRepeat: boolean
  _onStop: Function
  _onStart: Function
  _onUpdate: Function
  _onComplete: Function

  constructor(obj: Object) {
    this.obj          = obj
    this.target       = {}
    this.bk           = {}
    this.repeats      = 0
    this.time         = 0
    this.deltas       = {}
    this.fn           = Ticker.EASE.linear
    this.started      = false
    this.yoyoMode     = false
    this.alwaysRepeat = false
    tickers.push(this)
  }

  static EASE = {
    linear       : t => t,
    easeInQuad   : t => t * t,
    easeOutQuad  : t => t * (2 - t),
    easeInOutQuad: t => (t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t),
  }

  static reset() {
    tickers = []
  }

  static update() {
    tickers.forEach(t => {
      if (t.started) {
        t._tick()
      }
    })
  }

  static simulateTicks(count: number = 25) {
    for (let i = 0; i < count; i++) {
      Ticker.update()
    }
  }

  /***
   * Set new values
   *
   * @param {Object} targetObj - new values
   * @param {number} count - number of ticks
   * @returns {Ticker} for chaining
   */
  to(targetObj: Object, count: number = 1) {
    this.started  = false
    this.target   = { ...targetObj }
    this.keys     = Object.keys(targetObj)
    this.time     = 0
    this.timeStep = 1 / (count)
    this._backup()
    this.deltas = {}
    this.keys.forEach(key =>
      this.deltas[key] = targetObj[key] - this.obj[key])

    return this
  }

  _backup() {
    this.keys.forEach(key =>
      this.bk[key] = this.obj[key])
  }

  _restore() {
    this.keys.forEach(key =>
      this.obj[key] = this.bk[key])
  }

  /***
   *  Enable 'yoyo' mode
   *
   * @returns {Ticker} for chaining
   */
  yoyo() {
    this.yoyoMode = true

    return this
  }

  ease(fn: Function = Ticker.EASE.easeInOutQuad) {
    this.fn = fn
    return this
  }

  /***
   * Set repeats count
   *
   * @param {?number} count - repeats count, default is infinity
   * @returns {Ticker} for chaining
   */
  repeat(count: number = -1) {
    this.alwaysRepeat = count < 0
    this.repeats      = count

    return this
  }

  /***
   *  Pause ticker.
   *
   * @returns {Ticker} for chaining
   */
  pause() {
    this.started = false
    return this
  }

  /***
   *  Resume ticker.
   *
   * @returns {Ticker} for chaining
   */
  resume() {
    this.started = true
    return this
  }

  /***
   *  Stop ticker.
   *
   * @param {?Object} obj - replace values
   * @returns {Ticker} for chaining
   */
  stop(obj: ?Object = null) {
    this.started = false
    if (this._onStop) {
      this._onStop(obj || this.obj)
    }
    return this
  }

  /***
   *  Start ticker
   * @returns {Ticker} for chaining
   */
  start() {
    this.counter = 1
    this.started = true

    if (this._onStart) {
      this._onStart(this.obj)
    }
    return this
  }

  /***
   *  Set every tick callback
   *
   * @param {Function} fn - callback
   * @returns {Ticker} for chaining
   */
  onUpdate(fn: Function) {
    this._onUpdate = fn
    return this
  }

  /***
   *  Set start callback
   *
   * @param {Function} fn - callback
   * @returns {Ticker} for chaining
   */
  onStart(fn: Function) {
    this._onStart = fn
    return this
  }

  /***
   *  Set finish callback
   *
   * @param {Function} fn - callback
   * @returns {Ticker} for chaining
   */
  onComplete(fn: Function) {
    this._onComplete = fn
    return this
  }

  /***
   *  Set stop callback
   *
   * @param {Function} fn - callback
   * @returns {Ticker} for chaining
   */
  onStop(fn: Function) {
    this._onStop = fn
    return this
  }

  _complete() {
    this.started = false

    if (this._onComplete) {
      this._onComplete(this.obj)
    }
  }

  _restart() {
    this.started = false
    this.counter = 1
    this.time    = 0

    this.repeats--
    this._restore()
    this.started = true
  }

  _update(isLastTick: boolean = false) {
    if (isLastTick && !this.yoyoMode) {
      this.keys.forEach(key =>
        this.obj[key] = this.target[key])
    } else if (this.yoyoMode) {
      const time = this.time <= 0.5 ? this.time * 2.0 : (1 - this.time + this.timeStep) * 2.0
      this.keys.forEach(key =>
        this.obj[key] = this.bk[key] + this.fn(time) * this.deltas[key])
    } else {
      this.keys.forEach(key =>
        this.obj[key] = this.bk[key] + this.fn(this.time) * this.deltas[key])
    }

    if (this._onUpdate) {
      this._onUpdate(this.obj, this.counter++)
    }
  }

  _tick() {
    const isLastTick = this.time >= 1

    this._update(isLastTick)
    this.time += this.timeStep

    if (isLastTick) {
      if (this.alwaysRepeat || this.repeats > 1) {
        this._restart()
      } else {
        this._complete()
      }
    }
  }
}
