/* eslint-disable max-nested-callbacks */
import { sleep } from '../../helpers/async'
import {
  __getEventCollectors,
  __getEventsHistory,
  __getMailBox,
  __getSubscribers,
  emit,
  listen,
  on,
  popMail,
  pushMail,
  reset,
  setEventsHistorySize,
  watch,
  when,
} from '../Emit3r'

const initState = {
  rootPrimitive: 1,
  rootArray    : watch([1, 2, 3], 'ARRAY'),
  rootObject   : watch({
    nestedPrimitive: 1,
    nestedObject   : watch({ deepPrimitive: 3 }, 'DEEP'),
  }, 'ROOT_OBJ'),
}

const getStore = () => watch(initState)

const testDOMElement = document.createElement('div')
testDOMElement.id    = 'test-div'

describe('Emit3r Suite', () => {
  describe('-- function --', () => {
    beforeEach(() => reset())

    it('all listeners subscribed', () => {
      when('ADD', () => { })
      expect(__getSubscribers()).toMatchSnapshot()
    })

    it('every emit works', () => {
      const arr = []
      when('ADD', v => arr.push(v))
      emit('ADD', 1)
      emit('ADD', 2)
      emit('ADD', 4)
      expect(arr).toEqual([1, 2, 4])
    })

    it('all collectors subscribed', () => {
      when(['ADD', 'SUB'], () => { })
      expect(__getEventCollectors()).toMatchSnapshot()
      expect(__getSubscribers()).toEqual({})
    })

    it('collect uncomplete emit', () => {
      let v = 0
      when(['ADD', 'SUB'], () => v = 1)
      emit('ADD', 1)
      expect(v).toEqual(0)
    })

    it('collect complete emit', () => {
      let v = 0
      when(['ADD', 'SUB'], () => v = 100)
      emit('ADD', 1)
      emit('SUB', 1)
      expect(v).toEqual(100)
    })

    it('collect last args emit', () => {
      let v = 0
      when(['ADD', 'SUB'], (a) => v = a)
      emit('ADD')
      emit('SUB', 100)
      expect(v).toEqual({ ADD: [], SUB: [100] })
    })

    it('collect all args emit', () => {
      let v = 0
      when(['ADD', 'SUB'], (a) => v = a)
      emit('ADD', 50)
      emit('SUB', 100)
      expect(v).toEqual({ ADD: [50], SUB: [100] })
    })

    it('collect complete and uncomplete emit', () => {
      let v = 0
      when(['ADD', 'SUB'], () => v = 1)
      emit('ADD', 1)
      emit('SUB', 1)
      v = 0
      emit('SUB', 1)
      expect(v).toEqual(0)
    })

    it('defer emits', () => {
      const arr = []
      emit('ADD', 1)
      emit('ADD', 2)
      emit('ADD', 3)
      when('ADD', v => arr.push(v))
      expect(arr).toEqual([1, 2, 3])
    })

    it('setEventsHistorySize size 10', () => {
      setEventsHistorySize(0)
      setEventsHistorySize(10)
      emit('ADD', 1)
      emit('ADD', 1)
      emit('ADD', 1)
      emit('ADD', 1)
      emit('ADD', 1)
      expect(__getEventsHistory()).toMatchSnapshot()
    })

    it('setEventsHistorySize 3 before overload', () => {
      setEventsHistorySize(3)
      emit('ADD', 1)
      emit('ADD', 1)
      emit('ADD', 1)
      emit('ADD', 1)
      expect(__getEventsHistory()).toMatchSnapshot()
    })

    it('setEventsHistorySize 3 after overload', () => {
      emit('ADD', 1)
      emit('ADD', 1)
      emit('ADD', 1)
      emit('ADD', 1)
      setEventsHistorySize(3)
      expect(__getEventsHistory()).toMatchSnapshot()
    })

    it('setEventsHistorySize (2) overload - drop old event', () => {
      setEventsHistorySize(2)
      const arr = []
      emit('ADD', 1)
      emit('ADD', 2)
      emit('ADD', 4)
      when('ADD', v => arr.push(v))
      expect(arr).toEqual([2, 4])
    })

    it('DOM event', async () => {
      let val = 0
      when(testDOMElement, 'click', () => (val = 1))
      testDOMElement.click()
      await sleep(10)
      expect(val).toEqual(1)
    })

    it('every subscriber got event', () => {
      let value1 = 0
      let value2 = 0
      let value3 = 0
      when('ADD', v => value1 = value1 + v)
      when('ADD', v => value2 = value2 + v)
      when('ADD', v => value3 = value3 + v)
      emit('ADD', 1)
      emit('ADD', 2)
      emit('ADD', 4)
      expect([value1, value2, value3]).toEqual([7, 7, 7])
    })

    it('pushMail', () => {
      const mail = __getMailBox()
      pushMail('me', { event: 'ADD', args: 1 })
      expect(mail).toEqual({ me: [{ args: 1, event: 'ADD' }] })
    })

    it('popMail', () => {
      let value1 = 0
      let value2 = 0
      when('ADD', (v, w) => value1 += (v + w))
      when('ADD', (v, w) => value2 += (v + w))

      pushMail('me', { event: 'ADD', args: [1, 1] })
      pushMail('me', { event: 'ADD', args: [2, 1] })
      pushMail('me', { event: 'ADD', args: [4, 1] })
      popMail('me')
      popMail('me')
      popMail('me')
      popMail('me')
      expect(value1).toEqual(10)
      expect(value2).toEqual(10)
    })

    it('bulk pushMail', () => {
      let value1 = 0
      let value2 = 0
      when('ADD', v => value1 += v)
      when('ADD', v => value2 += v)

      pushMail('me', [
        { event: 'ADD', args: [1] },
        { event: 'ADD', args: [2] },
        { event: 'ADD', args: [4] },
      ])
      popMail('me')
      popMail('me')
      popMail('me')
      popMail('me')
      expect(value1).toEqual(7)
      expect(value2).toEqual(7)
    })

    it('store init value', () => {
      const store = getStore()
      expect(store).toMatchSnapshot()
    })

    it('got event after change value in store', () => {
      const store         = getStore()
      store.rootPrimitive = 'new'
      let newValue        = null
      when('CHANGE.rootPrimitive', v => newValue = v)
      expect(newValue).toEqual('new')
    })

    it('got event after add root key in store', () => {
      const store  = getStore()
      store.new    = 'newRoot'
      let newValue = null
      when('CHANGE.new', v => newValue = v)
      expect(newValue).toEqual('newRoot')
    })

    it('got event after change DEEP value in store', () => {
      const store  = getStore()
      let newValue = null

      store.rootObject.nestedObject.deepPrimitive = 'updated'
      when('CHANGE.DEEP.deepPrimitive', v => newValue = v)
      expect(newValue).toEqual('updated')
    })

    it('got event after replace array in store', () => {
      const store     = getStore()
      let newValue    = null
      store.rootArray = []
      when('CHANGE.rootArray', v => newValue = v)
      expect(newValue).toEqual([])
    })

    it('read array in store', () => {
      const store = getStore()
      store.arr   = [1, 2, 3]
      expect(store.arr[2]).toEqual(3)
    })

    it('got event after push to array in store', () => {
      const store = getStore()
      store.arr   = [1, 2, 3]
      store.arr.push(4)
      let newValue = null
      when('CHANGE.arr', v => newValue = v)
      expect(newValue).toEqual([1, 2, 3, 4])
    })

    it('got event after replace array value in store', () => {
      const store  = getStore()
      store.arr    = [1, 2, 3]
      store.arr[2] = 1
      let newValue = null
      when('CHANGE.arr', v => newValue = v)
      expect(newValue).toEqual([1, 2, 1])
    })
  })

  let inst
  let inst2

  @listen
  class TestClass {
    constructor(value = 0) {
      this.value      = value
      this.clicked    = false
      this.storeValue = null
    }

    @on(['ADD', 'SUB', 'MUL'])
    collectEvents(...args) {
      this.value   = args
      this.collect = true
    }

    @on('ADD')
    onEventAdd(delta) {
      this.value += delta
    }

    @on('SUB')
    onEventSub(delta) {
      this.value -= delta
    }

    @on('CHANGE.DEEP.deepPrimitive')
    onStore(value) {
      this.storeValue = value
    }

    @on(testDOMElement, 'click')
    onClick() {
      this.clicked = true
    }
  }

  describe('-- decortators --', () => {
    beforeEach(() => {
      reset()
      inst  = new TestClass()
      inst2 = new TestClass()
    })

    it('every instance subscribed', () => {
      const pool = __getSubscribers()
      expect(pool).toMatchSnapshot()
    })

    it('collectors subscribed', () => {
      expect(__getEventCollectors()).toMatchSnapshot()
    })

    it('collect complete emit', () => {
      emit('ADD', 1)
      emit('SUB', 1)
      emit('MUL', 1)
      expect(inst.collect).toEqual(true)
    })

    it('every instance got event', async () => {
      emit('ADD', 5)
      emit('SUB', 1)
      expect(inst.value).toEqual(4)
      expect(inst2.value).toEqual(4)
    })

    it('DOM event', async () => {
      testDOMElement.click()
      await sleep(10)
      expect(inst.clicked).toEqual(true)
      expect(inst2.clicked).toEqual(true)
    })

    it('store update', async () => {
      const store = getStore()

      store.rootObject.nestedObject.deepPrimitive = 'updated'
      expect(inst.storeValue).toEqual('updated')
      expect(inst2.storeValue).toEqual('updated')
    })
  })
})
