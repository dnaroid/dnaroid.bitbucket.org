import Ticker from '../Ticker'

beforeEach(() => {
  Ticker.reset()
})

const fixed = (arr) => arr.map(v => parseFloat(v.toFixed(3)))

describe('Ticker', () => {
  it('run only after start()', () => {
    const obj = { x: 0 }
    new Ticker(obj)
      .to({ x: 10 }, 5)
    Ticker.simulateTicks()
    expect(obj.x).toEqual(0)
  })

  it('increment', () => {
    const a = []
    const t = new Ticker({ x: 1 })
    t.to({ x: 5 }, 4).onUpdate(({ x }) => a.push(x)).start()
    Ticker.simulateTicks()
    expect(fixed(a)).toEqual([1, 2, 3, 4, 5])
  })

  it('parallel work', () => {
    const a1 = []
    const a2 = []
    const t1 = new Ticker({ x: 1 })
    const t2 = new Ticker({ x: 1 })
    t1.to({ x: 5 }, 4).onUpdate(({ x }) => a1.push(x)).start()
    t2.to({ x: 5 }, 4).onUpdate(({ x }) => a2.push(x)).start()
    Ticker.simulateTicks(3)
    expect(fixed(a1)).toEqual([1, 2, 3])
    expect(fixed(a2)).toEqual([1, 2, 3])
  })

  it('decrement', () => {
    const a = []
    const t = new Ticker({ x: 5 })
    t.to({ x: 1 }, 4).onUpdate(({ x }) => a.push(x)).start()
    Ticker.simulateTicks()
    expect(fixed(a)).toEqual([5, 4, 3, 2, 1])
  })

  it('many keys', () => {
    const obj = { x: 0, y: 0, z: 0 }
    new Ticker(obj).to({ x: 10, y: 6, z: 45 }, 5).start()
    Ticker.simulateTicks()
    expect(obj).toEqual({ x: 10, y: 6, z: 45 })
  })

  it('onUpdate', () => {
    const obj = { x: 1 }
    const a   = []
    new Ticker(obj).to({ x: 10 }, 9)
      .onUpdate(({ x }) => (a.push(x)))
      .start()
    Ticker.simulateTicks()
    expect(fixed(a)).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
  })

  it('should return target keys at last tick', () => {
    const a = []
    new Ticker({ x: 1 }).to({ x: 7 }, 5)
      .onUpdate(({ x }) => (a.push(x)))
      .start()
    Ticker.simulateTicks()
    expect(fixed(a)).toEqual([1, 2.2, 3.4, 4.6, 5.8, 7])
  })

  it('onUpdate value', () => {
    const obj = { x: 0 }
    let v     = 0
    new Ticker(obj).to({ x: 5 }, 5).onUpdate(({ x }) => (v = v + x)).start()
    Ticker.simulateTicks()
    expect(v).toEqual(15)
  })

  it('onUpdate index', () => {
    const obj = { x: 0 }
    const v   = []
    new Ticker(obj).to({ x: 15 }, 4).onUpdate((a, i) => (v.push(i))).start()
    Ticker.simulateTicks()
    expect(v).toEqual([1, 2, 3, 4, 5])
  })

  it('onComplete', () => {
    const obj = { x: 5 }
    let v     = 0
    new Ticker(obj).to({ x: 10 }, 5).onComplete(() => (v = 100)).start()
    Ticker.simulateTicks()
    expect(v).toEqual(100)
  })

  it('stop', () => {
    let a        = []
    const ticker = new Ticker({ x: 0 })
      .to({ x: 1 }, 10)
      .onUpdate((x, i) => a.push(i))
      .onComplete(() => (a = []))
      .start()
    Ticker.simulateTicks(5)
    ticker.stop()
    Ticker.simulateTicks(5)
    expect(a).toEqual([1, 2, 3, 4, 5])
  })

  it('stop with params', () => {
    let a        = { x: 1 }
    const ticker = new Ticker(a)
      .to({ x: 10 }, 10)
      .onStop(({ x }) => (a.x = x))
      .start()
    Ticker.simulateTicks(5)
    ticker.stop({ x: 20 })
    Ticker.simulateTicks(5)
    expect(a.x).toEqual(20)
  })

  it('onStop', () => {
    let a        = ''
    const ticker = new Ticker({ x: 0 })
      .to({ x: 1 }, 10)
      .onComplete(() => (a = 'onComplete'))
      .onStop(() => (a = 'onStop'))
      .start()
    Ticker.simulateTicks(5)
    ticker.stop()
    Ticker.simulateTicks(5)
    expect(a).toEqual('onStop')
  })

  it('onStart', () => {
    const obj = { x: 5 }
    let v     = 0
    new Ticker(obj).to({ x: 10 }, 5).onStart(() => (v = 100)).start()
    Ticker.simulateTicks()
    expect(v).toEqual(100)
  })

  it('reuse', () => {
    const a = []
    const t = new Ticker({ x: 1 })
    t.to({ x: 5 }, 4).onUpdate(({ x }) => a.push(x)).start()
    Ticker.simulateTicks()
    t.to({ x: 1 }, 4).start()
    Ticker.simulateTicks()
    expect(fixed(a)).toEqual([1, 2, 3, 4, 5, 5, 4, 3, 2, 1])
  })

  it('repeat', () => {
    const obj = { x: 0 }
    const a   = []
    new Ticker(obj)
      .to({ x: 3 }, 3)
      .onUpdate(({ x }) => (a.push(x)))
      .repeat(3)
      .start()
    Ticker.simulateTicks()
    expect(a).toEqual([0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3])
  })

  it('yoyo', () => {
    const a = []
    new Ticker({ x: 1 })
      .to({ x: 4 }, 6)
      .onUpdate(({ x }) => (a.push(x)))
      .yoyo()
      .start()
    Ticker.simulateTicks()
    expect(fixed(a)).toEqual([1, 2, 3, 4, 4, 3, 2, 1])
  })

  it('yoyo + repeat', () => {
    const a = []
    new Ticker({ x: 1 })
      .to({ x: 4 }, 6)
      .onUpdate(({ x }) => (a.push(x)))
      .repeat(2)
      .yoyo()
      .start()
    Ticker.simulateTicks()
    expect(fixed(a)).toEqual(
      [
        1, 2, 3, 4, 4, 3, 2, 1,
        1, 2, 3, 4, 4, 3, 2, 1,
      ],
    )
  })

  it('ease', () => {
    const a = []
    const t = new Ticker({ x: 1 })
    t.to({ x: 10 }, 9)
      .onUpdate(({ x }) => (a.push(x)))
      .ease(Ticker.EASE.easeInQuad)
      .start()
    Ticker.simulateTicks()
    expect(fixed(a)).toEqual([
      1,
      1.111,
      1.444,
      2,
      2.778,
      3.778,
      5,
      6.444,
      8.111,
      10,
    ])
  })

  it('ease reuse', () => {
    const a = []
    const o = { x: 1 }
    const t = new Ticker(o)
    t.to({ x: 10 }, 4)
      .onUpdate(({ x }) => (a.push(x)))
      .ease(Ticker.EASE.easeInQuad)
      .start()

    Ticker.simulateTicks()

    t.to({ x: 1 }, 4).start()

    Ticker.simulateTicks()
    expect(fixed(a)).toEqual([
      1,
      1.563,
      3.25,
      6.063,
      10,
      10,
      9.438,
      7.75,
      4.938,
      1,
    ])
  })
})
