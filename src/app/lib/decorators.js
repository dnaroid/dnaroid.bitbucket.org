const logCall = (name, result, time, args) => {
  const isArgs   = args.length > 0
  const isResult = result !== undefined

  console.debug(name,
    isArgs ? '(' : '()',
    isArgs ? args : '',
    isArgs ? ')' : '',
    isResult ? ' ->' : '',
    isResult ? result : '',
    ` - ${time}ms`,
  )
}

export function log() {
  return function (target, name, descriptor) {
    return ({
      ...descriptor,
      value(...args) {
        const startTime  = new Date()
        const result     = descriptor.value.apply(this, args)
        const finishTime = new Date()
        logCall(`${target.constructor.name}.${name}`, result, finishTime - startTime, args)
        return result
      },
    })
  }
}

const memoized = new WeakMap()

function memoizationFor(obj) {
  let table = memoized.get(obj)
  if (!table) {
    table = Object.create(null)
    memoized.set(obj, table)
  }
  return table
}

function memoize(_, name, descriptor) {
  const getter = descriptor.get
  const setter = descriptor.set

  descriptor.get = function () {
    const table = memoizationFor(this)
    if (name in table) { return table[name] }
    return table[name] = getter.call(this)
  }

  descriptor.set = function (val) {
    const table = memoizationFor(this)
    setter.call(this, val)
    table[name] = val
  }
}

function getPropertyDescriptor(obj, property) {
  if (obj === null) {
    return null
  }
  if (obj.hasOwnProperty(property)) {
    return Object.getOwnPropertyDescriptor(obj, property)
  }
  return getPropertyDescriptor(Object.getPrototypeOf(obj), property)
}

function logMethodCall(name, target, context, args) {
  const startTime  = new Date()
  const result     = target.apply(context, args)
  const finishTime = new Date()
  logCall(name, result, finishTime - startTime, args)
  return result
}

export function logAll(target) {
  Object.getOwnPropertyNames(target.prototype)
    .forEach(methodName => {
      const name       = `${target.name}.${methodName}`
      const descriptor = getPropertyDescriptor(target.prototype, methodName)

      if (descriptor.value) {
        const originalMethod = target.prototype[methodName]
        if (methodName !== 'constructor') {
          const handler                = {
            apply: (targ, context, args) => logMethodCall(name, targ, context, args),
          }
          target.prototype[methodName] = new Proxy(originalMethod, handler)
        }
      } else {
        let accessor
        if (descriptor.set) {
          accessor = 'set'
        } else if (descriptor.get) {
          accessor = 'get'
        }
        const method            = descriptor[accessor]
        descriptor[accessor]    = function (...args) {
          return logMethodCall(name, method, this, args)
        }
        descriptor.configurable = true
        Object.defineProperty(target.prototype, methodName, descriptor)
      }
    })
}
