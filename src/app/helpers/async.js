export function debounce(func, wait, immediate) {
  let timeout
  return function () {
    const context = this
    const args    = arguments
    const later   = function () {
      timeout = null
      if (!immediate) { func.apply(context, args) }
    }
    const callNow = immediate && !timeout
    clearTimeout(timeout)
    timeout = setTimeout(later, wait)
    if (callNow) { func.apply(context, args) }
  }
}

export function throttle(fn, threshhold = 250, scope) {
  let last, deferTimer

  return function () {
    const context = scope || this

    const now  = +new Date(),
          args = arguments

    if (last && now < last + threshhold) {
      clearTimeout(deferTimer)
      deferTimer = setTimeout(() => {
        last = now
        fn.apply(context, args)
      }, threshhold)
    } else {
      last = now
      fn.apply(context, args)
    }
  }
}

export const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

export const later = (fn) => setTimeout(fn)
