import * as THREE from 'three'
import { Store } from '../model'

const direction = new THREE.Vector3()
const raycaster = new THREE.Raycaster()

export function isFreeRay(myId, targetId) {
  const myPos      = Store.bodyById[myId].getWorldPosition()
  const targetMesh = Store.bodyById[targetId].__target
  const targetPos  = targetMesh.getWorldPosition()
  const angle      = direction.subVectors(targetPos, myPos).normalize()

  raycaster.set(myPos, angle)

  const [firstSeen] = raycaster
    .intersectObjects([...Store.board.env.children, targetMesh], false)

  return firstSeen && firstSeen.object && firstSeen.object.__unitId === targetId
}
