// @flow
import {
  UNIT_LOOK_TO,
  UNIT_MOVE_TO,
  UNIT_START_MOVE,
  UNIT_STOP_MOVE
} from '../model/Event'
import Unit from '../core/Unit'
import { clearMail, pushMail } from '../lib/Emit3r'

export function compilePathFor(unit: Unit) {
  const { path, id } = unit

  if (path.size > 0) {
    const head = { event: UNIT_START_MOVE, args: [unit] }
    let order  = []

    clearMail(id)
    path.compress()
    path.steps.forEach((step, idx) => {
      const { x, y, dx = 0, dy = 0, look, fire, strafe, run } = step

      order = idx === 0 ? [head] : []
      if (look) {
        order.push({ event: UNIT_LOOK_TO, args: [unit, look.x, look.y] })
      } else if (fire) {
        order.push({ event: UNIT_LOOK_TO, args: [unit, fire.x, fire.y] })
      } else if (strafe) {
        order.push({ event: UNIT_MOVE_TO, args: [unit, x, y, run] })
      } else {
        order.push({ event: UNIT_LOOK_TO, args: [unit, x + dx, y + dy] })
        order.push({ event: UNIT_MOVE_TO, args: [unit, x, y, run] })
      }
      pushMail(id, order)
    })
    pushMail(id, { event: UNIT_STOP_MOVE, args: [unit] })
    unit.path.clear()
  }
}

