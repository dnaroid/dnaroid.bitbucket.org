export const formatTime = (ms) => {
  const sec   = Math.floor(ms)
  const hours = Math.floor(sec / 3600)
  let minutes = Math.floor((sec - (hours * 3600)) / 60)
  let seconds = sec - (hours * 3600) - (minutes * 60)
  let msec    = ~~(100 * (ms - sec))

  if (minutes < 10) {minutes = `0${minutes}`}
  if (seconds < 10) {seconds = `0${seconds}`}
  if (msec < 10) {msec = `0${msec}`}
  return `${minutes}:${seconds}.${msec}`
}
