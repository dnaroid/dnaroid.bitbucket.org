import { Const, Store } from '../model'
import { when } from '../lib/Emit3r'
import { RESOURCES_LOADED } from '../model/Event'

const CELL_SIZE    = Const.SIZE.BOARD_CELL
const halfCellSize = CELL_SIZE / 2

let halfWorldWidth
let halfWorldHeight
let halfBoardWidth
let halfBoardHeight

when(RESOURCES_LOADED, () => {
  const { board: { width, height, worldWidth, worldHeight } } = Store.resources

  halfWorldWidth  = worldWidth / 2
  halfWorldHeight = worldHeight / 2
  halfBoardWidth  = width / 2
  halfBoardHeight = height / 2
})

export const boardToWorldX = bx =>
  ((bx - halfBoardWidth) * CELL_SIZE + halfCellSize)

export const boardToWorldZ = by =>
  ((by - halfBoardHeight) * CELL_SIZE + halfCellSize)

export const cursorToBoardX = wx =>
  Math.floor((wx + halfWorldWidth) / CELL_SIZE)

export const cursorToBoardY = wy =>
  Math.floor((wy + halfWorldHeight) / CELL_SIZE)
