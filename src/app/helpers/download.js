export default function download(fileName, data) {
  const a    = window.document.createElement('a')
  a.href     = window.URL.createObjectURL(new Blob([data], { type: 'text/csv' }))
  a.download = fileName
  document.body.appendChild(a)
  a.clickBoard()
  document.body.removeChild(a)
}
