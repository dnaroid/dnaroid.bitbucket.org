const COLOR = {
  DEFAULT: '#595959',
  GLOW   : '#ffffff',
}

const ICON = {
  WIDTH : 16,
  HEIGHT: 16,
}

const mouseIcon = ({ left, right, middle }) => `
  <svg xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
  xmlns="http://www.w3.org/2000/svg" 
  height="${ICON.HEIGHT}px" width="${ICON.WIDTH}px" 
  version="1.1" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" 
  viewBox="0 0 188.6543 240.07227"
  class="icon">
  <g transform="translate(-197 -285)">
  <path style="fill:${right}" d="m307 285v26.3c14 7.44 23.8 24.5 23.8 44.4v33.6h54.7v-25.1c0-43.6-34.9-78.8-78.5-79.2z"/>
  <path style="fill:${left}" d="m276 285c-43.5 0.402-78.5 35.6-78.5 79.2v25.1h54.7v-33.6c0-19.9 9.76-36.9 23.8-44.4v-26.3z"/>
  <path style="fill:${COLOR.DEFAULT}" d="m197 421v25.1c0 43.9 35.3 79.2 79.2 79.2h30.2c43.9 0 79.2-35.3 79.2-79.2v-25.1h-189z"/>
  <rect style="fill:${middle}" rx="18.6" ry="19.8" height="77.2" width="37.2" y="329" x="273"/>
  </g>
  </svg>
`

export const LMB = mouseIcon({ left: COLOR.GLOW, right: COLOR.DEFAULT, middle: COLOR.DEFAULT })
export const RMB = mouseIcon({ right: COLOR.GLOW, left: COLOR.DEFAULT, middle: COLOR.DEFAULT })
export const MMB = mouseIcon({ middle: COLOR.GLOW, left: COLOR.DEFAULT, right: COLOR.DEFAULT })

export const ARROWS = `
  <svg xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#' xmlns='http://www.w3.org/2000/svg' 
  height='${ICON.HEIGHT}EXTERNAL_FRAGMENTpx' width="${ICON.WIDTH}px" version="1.1" xmlns:cc="http://creativecommons.org/ns#" 
  xmlns:dc="http://purl.org/dc/elements/1.1/" 
  viewBox="0 0 188.6543 240.07227"
  class="icon">
  <path style="fill:#a9a9a9" d="m94.4 23.7l-39.1 39.1h28.8v44.2h-44.5v-28.6l-38.1 38.6 38.1 38v-31h44.5v45h-30.5l40.8 40 40.6-40h-32v-45h44v33l40-40-40-40.5v30.5h-44v-44.2h31l-39.6-39.1z" transform="translate(0 -.178)"/>
  </svg>
`

export const ARROW_LEFT = `
  <svg xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
  xmlns="http://www.w3.org/2000/svg" 
  transform="translate(0 3)"
  height="${ICON.HEIGHT}px" viewBox="0 0 72.926693 185.61037" 
  width="${ICON.WIDTH / 3}px" version="1.1" 
  xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <path style="fill:#a9a9a9" d="m47.5-0.137v140l-47.5-70.1z"/>
  </svg>
`

export const ARROW_RIGHT = `
  <svg xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
  xmlns="http://www.w3.org/2000/svg" 
  transform="translate(0 3)"
  height="${ICON.HEIGHT}px" viewBox="0 0 72.926693 185.61037" 
  width="${ICON.WIDTH / 3}px" version="1.1" 
  xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <path style="fill:#a9a9a9" d="m0-0.137 0.0000013 140 47.5-70.1z"/>
  </svg>
`

export const KEY = (text) => `
  <svg xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#' 
  xmlns='http://www.w3.org/2000/svg' 
  height='${ICON.HEIGHT}px' width="${ICON.WIDTH * (text.length > 1 ? text.length * 0.6 : 1)}px" 
  version="1.1" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" 
  viewBox="0 0 188.6543 240.07227"
  class="icon">
  <g transform="translate(${-107 - (text.length > 1 ? text.length * 60 : 90)} -285)">
  <rect style="fill:#888888" rx="16.4" ry="19.8" height="225" width="${110 + text.length * 100}" y="296" x="199"/>
  <rect style="fill:#444444" rx="18.6" ry="19.8" height="171" width="${80 + text.length * 100}" y="315" x="210"/>
  <text style="font-size:180px;letter-spacing:0px;line-height:125%;word-spacing:0px;font-family:monospace;fill:#ffffff" xml:space="preserve" y="452.84973" x="232.59656"><tspan x="232.59656" y="452.84973">${text}</tspan></text>
  </g>
  </svg>
`
export const PAUSE = `
  <svg xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#' xmlns='http://www.w3.org/2000/svg' 
  height='40px' width='40px' 
  version='1.1' xmlns:cc='http://creativecommons.org/ns#' xmlns:dc='http://purl.org/dc/elements/1.1/' 
  viewBox='0 0 188.6543 240.07227'>
  <g transform='translate(-197 -285)' style='fill:#fffa70'>
  <rect rx='30.1' ry='30.1' height='238' width='77.1' y='287' x='199'/>
  <rect rx='30.1' ry='30.1' height='238' width='77.1' y='287' x='309'/>
  </g>
  </svg>
`

export const PLAY = `
  <svg xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns="http://www.w3.org/2000/svg" 
  height="40px" width="40px" 
  id='svg_play'
  version="1.1" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" 
  viewBox="0 0 188.6543 240.07227">
  <g transform="translate(-197 -285)">
  <path style="fill:#fffa70" d="m229 297 145 102c5.9 4.14 5.9 10.8 0 14.9l-145 102c-11.7 8.2-21.3 8.77-21.3-6v-206c0-16.2 7.53-15.7 21.3-6z"/>
  </g>
  </svg>
`

const cursor = (color) => `
<svg xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns="http://www.w3.org/2000/svg" 
height="24" width="24" transform='translate(12 12)'
version="1.1" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" viewBox="0 0 24.019407 23.983883">
<path style="stroke-linejoin:round;stroke:${color};stroke-width:3;fill:none" d="m1.63 1.73 20.7 7.63-9.8 2.14-2.2 10.9z"/>
</svg>
`

const target = `
  <svg xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns="http://www.w3.org/2000/svg" height="24" width="24" version="1.1" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" viewBox="0 0 24.019407 23.983883">
  <ellipse style="stroke-linejoin:round;stroke:#f00;stroke-width:1.85;fill:none" rx="8.05" ry="8.04" cy="12.1" cx="12"/>
  <path style="fill:#f00" d="m9.12-0.0384 5.79-0.000001-2.89 9.2z"/>
  <path style="fill:#f00" d="m14.9 24-5.79 0.000001 2.89-9.2z"/>
  <path style="fill:#f00" d="m24 9.09 0.000001 5.78-9.21-2.88z"/>
  <path style="fill:#f00" d="m9.26e-7 14.9-9.26e-7 -5.81 9.21 2.91z"/>
  </svg>
  `

export const CURSOR = {
  TARGET : target,
  DEFAULT: cursor('#ffff00'),
  ENEMY  : cursor('#ff0000'),
  ALLY   : cursor('#00ff00'),
}
