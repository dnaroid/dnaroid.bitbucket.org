const ACCURACY = 4 // for tracks dropping

function isActiveTrack(track) {
  for (let i = 4; i < track.values.length - 4; i = i + 4) {
    for (let s = 0; s < 4; s++) {
      if (track.values[i + s].toFixed(ACCURACY) !== track.values[s].toFixed(ACCURACY)) {
        return true
      }
    }
  }
  return false
}

export function getActiveTracks(tracks) {
  const result = []
  tracks.forEach(track => {
    if (isActiveTrack(track)) { result.push(track) }
  })
  return result
}

