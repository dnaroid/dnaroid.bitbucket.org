export const inRange = (a, min, max) =>
  (a = a > max ? max : a < min ? min : a)

export const updateInRange = (obj, field, delta, min, max) => {
  obj[field] = obj[field] + delta
  if (obj[field] > max) {
    obj[field] = max
    return false
  }
  if (obj[field] < min) {
    obj[field] = min
    return false
  }
  return true
}

export const lockInRange = (obj, field, min, max) => {
  if (obj[field] > max) {
    obj[field] = max
    return false
  }
  if (obj[field] < min) {
    obj[field] = min
    return false
  }
  return true
}

export const rotateAroundY = (obj, cx, cz, angle) => {
  const s        = Math.sin(angle)
  const c        = Math.cos(angle)
  obj.position.x -= cx
  obj.position.z -= cz
  const xnew     = obj.position.x * c - obj.position.z * s
  const ynew     = obj.position.x * s + obj.position.z * c
  obj.position.x = xnew + cx
  obj.position.z = ynew + cz
}

export const distance = (x1, y1, x2, y2) => {
  const a = x1 - x2
  const b = y1 - y2
  return Math.sqrt(a * a + b * b)
}

export const getAngleTo = (x, y, xt, yt) => {
  const angle = Math.atan2(xt - x, yt - y)
  return angle < 0 ? angle + 2 * Math.PI : angle
}

export const getDeltaAngle = (angle1, angle2) =>
  Math.atan2(Math.sin(angle2 - angle1), Math.cos(angle2 - angle1))

export const isInFOV = ({ angle, x, y }, { x: xt, y: yt }) =>
  (Math.abs(getDeltaAngle(angle, getAngleTo(x, y, xt, yt))) < (Math.PI / 3))
