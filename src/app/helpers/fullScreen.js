export const enterFullScreen = () => {
  const elem = document.body
  if (elem.requestFullscreen) {
    elem.requestFullscreen()
  } else if (elem.mozRequestFullScreen) {
    elem.mozRequestFullScreen()
  } else if (elem.webkitRequestFullscreen) {
    elem.webkitRequestFullscreen()
  }
}

export const exitFullScreen = () => {
  const elem = document.body
  if (elem.cancelFullScreen) {
    elem.cancelFullScreen()
  } else if (elem.mozCancelFullScreen) {
    elem.mozCancelFullScreen()
  } else if (elem.webkitCancelFullScreen) {
    elem.webkitCancelFullScreen()
  } else if (elem.webkitExitFullscreen) {
    elem.webkitExitFullscreen()
  }
}
