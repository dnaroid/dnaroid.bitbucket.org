export default {

  error(...args) {console.error(...args)},

  debug(...args) {console.debug(...args)},

  warn(...args) {console.warn(...args)},

  info(...args) {console.info(...args)},

  progress(xhr) {
    if (xhr.lengthComputable) {
      const fileName        = xhr.target.responseURL
      const percentComplete = xhr.loaded / xhr.total * 100
      console.debug(`${fileName}: ${Math.round(percentComplete, 2)}% downloaded`)
    }
  },
}

