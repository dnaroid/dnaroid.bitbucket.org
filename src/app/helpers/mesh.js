const needsUpdate = (material, geometry) => {
  return function () {
    material.shading            = +material.shading // Ensure number
    material.vertexColors       = +material.vertexColors // Ensure number
    material.side               = +material.side // Ensure number
    material.needsUpdate        = true
    geometry.verticesNeedUpdate = true
    geometry.normalsNeedUpdate  = true
    geometry.colorsNeedUpdate   = true
  }
}

export const updateTexture = (material, materialKey, textures) => {
  return function (key) {
    material[materialKey] = textures[key]
    material.needsUpdate  = true
  }
}

export const update = (mesh) => {
  needsUpdate(mesh.material, mesh.geometry)
}

export function hexToRGB(hex) {
  const bigint = parseInt(hex, 16)
  const r      = (bigint >> 16) & 255
  const g      = (bigint >> 8) & 255
  const b      = bigint & 255
  return [r, g, b]
}