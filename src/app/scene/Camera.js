// @flow
import * as THREE from 'three'

import Store from '../model/Store'
import Config from '../model/Config'
import {
  CAMERA_MOVE,
  CAMERA_ROTATE_X,
  CAMERA_ROTATE_Y,
  CAMERA_SET_FLOOR_POS,
  CAMERA_TRANSLATE_X,
  CAMERA_TRANSLATE_Y,
  CAMERA_TRANSLATE_Z,
  CAMERA_ZOOM
} from '../model/Event'
import { rotateAroundY, updateInRange } from '../helpers/math'
import { listen, on } from '../lib/Emit3r'

const {
        rotateXSpeed,
        rotateXMoveZSpeed,
        rotateYSpeed,
        rotXMin,
        rotXMax,
        zoomMin,
        zoomMax,
        zoomSpeed,
      } = Config.controls

@listen
export default class Camera {
  floorX: number
  floorY: number
  camera: Object
  holder: Object
  renderer: Object

  constructor() {
    this.renderer = Store.renderer.threeRenderer
    const width   = this.renderer.domElement.width
    const height  = this.renderer.domElement.height
    this.floorX   = 0
    this.floorY   = 0

    this.camera = new THREE.PerspectiveCamera(
      Config.camera.fov,
      width / height, Config.camera.near,
      Config.camera.far)

    this.camera.zoom = Config.camera.zoom

    this.holder = new THREE.Group()
    this.holder.add(this.camera)
    Store.scene.add(this.holder)

    this.init()
  }

  init() {
    window.addEventListener('resize', () => this.updateSize(), false)
    this.updateSize()

    const [px, py, pz] = Config.camera.position
    const [rx, ry]     = Config.camera.rotation

    this.camera.position.set(0, py, 0)
    this.camera.rotation.set(rx, 0, 0)
    this.holder.position.set(px, 0, pz)
    this.holder.rotation.set(0, ry, 0)
  }

  updateSize() {
    // Multiply by dpr in case it is retina device
    this.camera.aspect = this.renderer.domElement.width * Config.dpr / this.renderer.domElement.height * Config.dpr
    this.camera.updateProjectionMatrix()
  }

  @on(CAMERA_SET_FLOOR_POS)
  setFloorPos(x: number, y: number) {
    this.floorX = x
    this.floorY = y
  }

  @on(CAMERA_MOVE)
  move(dx: number, dy: number) {
    this.holder.translateX(dx)
    this.holder.translateZ(dy)
  }

  @on(CAMERA_ROTATE_Y)
  rotateY(delta: number) {
    this.holder.rotateY(-delta * rotateYSpeed)
    rotateAroundY(this.holder, this.floorX, this.floorY, delta * rotateYSpeed)
  }

  @on(CAMERA_ROTATE_X)
  rotateX(delta: number) {
    const changed: boolean = updateInRange(this.camera.rotation, 'x', delta * rotateXSpeed, rotXMin, rotXMax)
    if (changed) {
      this.holder.translateZ(delta * rotateXMoveZSpeed)
    }
  }

  @on(CAMERA_ZOOM)
  zoom(delta: number) {
    updateInRange(this.camera, 'zoom', -delta * zoomSpeed, zoomMin, zoomMax)
    this.camera.updateProjectionMatrix()
  }

  @on(CAMERA_TRANSLATE_X)
  translateX(distance: number) {
    this.holder.translateX(distance)
  }

  @on(CAMERA_TRANSLATE_Y)
  translateY(distance: number) {
    this.holder.translateY(distance)
  }

  @on(CAMERA_TRANSLATE_Z)
  translateZ(distance: number) {
    this.holder.translateZ(distance)
  }

}
