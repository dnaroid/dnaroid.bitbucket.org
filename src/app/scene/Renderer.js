import * as THREE from 'three'
import { Config, Store } from '../model'

export default class Renderer {
  constructor() {
    // Properties
    this.scene     = Store.scene
    this.container = Store.container

    // Create WebGL renderer and set its antialias
    this.threeRenderer = new THREE.WebGLRenderer({ antialias: true })

    // Set clear color to fog to enable fog or to hex color for no fog
    this.threeRenderer.setClearColor(0x000000, 1)
    this.threeRenderer.setPixelRatio(window.devicePixelRatio) // For retina

    // Appends canvas
    this.container.appendChild(this.threeRenderer.domElement)

    // Shadow map options
    this.threeRenderer.shadowMap.enabled = true
    this.threeRenderer.shadowMap.type    = THREE.PCFSoftShadowMap

    // Get anisotropy for textures
    Config.maxAnisotropy = this.threeRenderer.getMaxAnisotropy()

    // Initial size update set to canvas container
    this.updateSize()

    // Listeners
    document.addEventListener('DOMContentLoaded', () => this.updateSize(), false)
    window.addEventListener('resize', () => this.updateSize(), false)
  }

  updateSize() {
    document.getElementById('appContainer').style.height = `${window.innerHeight - Config.HUD.height}px`
    document.getElementById('hud').style.height          = `${Config.HUD.height}px`
    this.threeRenderer.setSize(this.container.offsetWidth, this.container.offsetHeight)
  }

  render(scene, camera) {
    // Renders scene to canvas target
    this.threeRenderer.render(scene, camera)
  }
}
