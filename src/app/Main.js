import * as THREE from 'three'
import { Config, Store } from './model'
import AI from './controller/AI'
import FOV from './controller/FOV'
import HUD from './view/HUD/'
import Mouse from './controller/Mouse'
import Light from './scene/Light'
import Board from './view/Board'
import Ticker from './lib/Ticker'
import Camera from './scene/Camera'
import MiniMap from './view/MiniMap'
import Cursor3D from './controller/Cursor3D'
import Renderer from './scene/Renderer'
import Keyboard from './controller/Keyboard'
import ResLoader from './core/Loader'
import PathFinder from './core/PathFinder'
import UnitsManager from './core/UnitsManager'
import ModelFactory from './view/ModelFactory'
import CameraControl from './controller/CameraControl'
import {
  emit,
  listen,
  logAllEmits,
  on,
  spy,
  __getSubscribers,
} from './lib/Emit3r'
import { RESOURCES_LOADED } from './model/Event'
import Timer from './controller/Timer'

// #if DEBUG
console.log('--- DEVELOPMENT MODE ---')
//enableDebug()
//logAllEmits()
//spy('update|unit|game')
// #endif

@listen
export default class Main {
  constructor() {
    if (window.devicePixelRatio) { // for retina
      Config.dpr = window.devicePixelRatio
    }
    Store.container = document.getElementById('appContainer')
    Store.clock     = new THREE.Clock()
    Store.scene     = new THREE.Scene()
    Store.scene.fog = new THREE.FogExp2(Config.fog.color, Config.fog.near)
    Store.renderer  = new Renderer()
    Store.light     = new Light()
    Store.camera    = new Camera()

    new AI()
    new FOV()
    new HUD()
    new Mouse()
    new Timer()
    new Board()
    new MiniMap()
    new Keyboard()
    new Cursor3D()
    new ResLoader()
    new PathFinder()
    new ModelFactory()
    new UnitsManager()
    new CameraControl()
  }

  @on(RESOURCES_LOADED)
  start() {
    // #if DEBUG
    window.Store = Store
    window.emit  = emit
    console.log(' ---Store ---', Store)
    console.log(' --- Subscribers ---', __getSubscribers())
    // #endif
    this.render()
  }

  render() {
    const delta = Store.clock.getDelta()

    Ticker.update()
    Store.mixers.forEach(mixer => mixer.update(delta))
    MiniMap.update()
    CameraControl.update()
    Store.renderer.render(Store.scene, Store.camera.camera)
    if (!Store.isPaused) {
      Timer.update(delta)
    }

    requestAnimationFrame(this.render.bind(this))
  }
}

