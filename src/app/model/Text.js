import { ARROW_LEFT, ARROW_RIGHT, KEY, LMB, MMB, RMB } from '../helpers/svg'

const SEPARATOR = '<span class="separator"></span>'

export default {
  hint: {
    selectUnit: `${LMB}Accept move/fire${SEPARATOR}
                 ${KEY('Alt')}${LMB}Strafe${SEPARATOR}
                 ${KEY('Shift')}${LMB}Run${SEPARATOR}
                 ${RMB}Look${SEPARATOR}
                 ${KEY('space')}Toggle pause`,

    default: `${ARROW_LEFT}${LMB}${ARROW_RIGHT}Move camera${SEPARATOR}
              ${ARROW_LEFT}${RMB}${ARROW_RIGHT}Rotate camera${SEPARATOR}
              ${MMB}Zoom${SEPARATOR} 
              ${KEY('1')}...${KEY('4')}Select unit${SEPARATOR}
              ${KEY('space')}Toggle pause`,
  },
}
