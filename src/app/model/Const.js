const Const = {
  KIND: {
    WALL: 1,
    CORN: 2,
  },

  TEAM: {
    RED : 0,
    BLUE: 1,
  },

  UNIT: {
    STATE: {
      IDLE    : 0,
      ROTATION: 1,
      MOVE    : 2,
    },
  },

  TIME: {
    CLICK_MS_MAX: 100,
  },

  DEG: {
    D90: Math.PI / 2,
    D60: Math.PI / 3,
    D45: Math.PI / 4,
  },

  COLOR: {
    CURSOR_3D: {
      DEFAULT : '#ffffff',
      ALLY    : '#00ff00',
      ENEMY   : '#ff0000',
      SELECTED: '#ffd900',
    },

    CURSOR_2D: {
      DEFAULT : 'default',
      ALLY    : 'ally',
      ENEMY   : 'enemy',
      TARGET  : 'target',
      SELECTED: 'default',
    },

    PATH: {
      MOVE      : '#ffffff',
      STRAFE    : '#00ff00',
      RUN       : '#0000ff',
      RUN_STRAFE: '#00ffff',
      LOOK      : '#FFFF00',
      FIRE      : '#FF0000',
      PREVIEW   : '#ff00ff',
    },

    MAP: {
      WALL: '#00577f',
      UNIT: {
        [true] : '#ff8b00',
        [false]: '#9eff00',
      },
    },

    TEAM: [
      '#7f0000',
      '#00007f',
    ],
  },

  OPACITY: {
    CURSOR_3D     : 0.2,
    WALL          : 0.5,
    BOARD_CELL_MAX: 0.5,
    BOARD_CELL_MIN: 0.1,
  },

  SIZE: {
    CURSOR_3D     : 0.4,
    BOARD_CELL    : 7,
    WALL_THICKNESS: 1,
    WALL_HEIGHT   : 15,
    PATH_WIDTH    : 0.5,
    PATH_LENGTH   : 5,
  },

  POSITION: {
    PATH_Y: 0.5,
  },

  KEY: {
    UP   : 38,
    DOWN : 40,
    LEFT : 37,
    RIGHT: 39,
    SHIFT: 16,
    ALT  : 18,
    CTRL : 17,
  },

  MODE_SELECT: {
    UNIT  : 1,
    TARGET: 2,
  },

  BUTTON: {
    LEFT  : 0,
    MIDDLE: 1,
    RIGHT : 2,
  },

  /*

   1--2--3      -5-
   |     |    |  |  |
   21         24-25-26
   |     |    |  |  |
   41- -43      -45-

   */

  MAP: {
    TILES: {
      0 : {},
      30: { unit: { team: 1, angle: 0 } },
      16: { unit: { team: 0, angle: Math.PI } },
      1 : {
        vertical  : { start: true, length: 0.5 },
        horizontal: { start: true, length: 0.5 },
      },
      2 : {
        horizontal: { start: true, length: 1 },
      },
      3 : {
        vertical  : { start: true, length: 0.5 },
        horizontal: { end: true, length: 0.5 },
      },
      5 : {
        vertical  : { start: true, length: 0.5 },
        horizontal: { start: true, length: 1 },
      },
      21: {
        vertical: { start: true, length: 1 },
      },
      24: {
        vertical  : { start: true, length: 1 },
        horizontal: { start: true, length: 0.5 }
      },
      25: {
        vertical  : { start: true, length: 1 },
        horizontal: { start: true, length: 1 }
      },
      26: {
        vertical  : { start: true, length: 1 },
        horizontal: { end: true, length: 0.5 }
      },
      41: {
        vertical  : { end: true, length: 0.5 },
        horizontal: { start: true, length: 0.5 },
      },
      43: {
        vertical  : { end: true, length: 0.5 },
        horizontal: { end: true, length: 0.5 },
      },
      45: {
        vertical  : { end: true, length: 0.5 },
        horizontal: { start: true, length: 1 },
      },
    },

    PIXELS: {
      1 : [0, 0, 1, 0, 0, 1],
      2 : [0, 0, -1, 0, 1, 0],
      3 : [0, 0, -1, 0, 0, 1],
      5 : [0, 0, -1, 0, 1, 0, 0, 1],
      21: [0, 0, 0, 1, 0, -1],
      24: [0, 0, 1, 0, 0, -1, 0, 1],
      25: [0, 0, -1, 0, 1, 0, 0, -1, 0, 1],
      26: [0, 0, -1, 0, 0, -1, 0, 1],
      41: [0, 0, 1, 0, 0, -1],
      43: [0, 0, -1, 0, 0, -1],
      45: [0, 0, -1, 0, 1, 0, 0, -1],
    },
  },

  LINE: {
    WHITE: 1,
    RED  : 2,
    GREEN: 3,
    BLUE : 4,
  },
}

export default Const
