import { CURSOR } from '../helpers/svg'

const Config = {
  tourTime: 5,

  path: {
    models  : '/assets/models/',
    images  : '/assets/images/',
    textures: '/assets/textures/',
    shaders : '/assets/shaders/',
    maps    : '/assets/maps/',
  },

  HUD: {
    height: 100,
    map   : {
      height: 100,
      width : 100,
    },
  },

  controls: {
    rotateXSpeed     : 1,
    rotateXMoveZSpeed: 100,
    rotateYSpeed     : 2,
    keyMoveSpeed     : 5,
    dragSpeed        : 50,
    fovMin           : 20,
    fovMax           : 90,
    rotXMin          : -Math.PI / 2,
    rotXMax          : -Math.PI / 4,
    posYMin          : 35,
    posYMax          : 200,
    zoomMin          : 0.1,
    zoomMax          : 1,
    zoomSpeed        : 0.00005,
    borderSensors    : false,
  },

  resources: {
    levels  : ['0'],
    models  : [],
    textures: {
      // floor: 'scifi/sand_01',
      // floorNormal: 'scifi/sand_01_normal',
      floor      : 'scifi/remn_metalfloor3',
      floorNormal: 'scifi/remn_metalfloor3_normal',
      wall       : 'scifi/remn_metal_p9',
      wallNormal : 'scifi/remn_metal_p9_normal',
    },

    shaders: [],

    sprites: {},

    cursor: {
      target : CURSOR.TARGET,
      enemy  : CURSOR.ENEMY,
      default: CURSOR.DEFAULT,
      ally   : CURSOR.ALLY,
    },
  },

  unit: {
    modelName: 'girl',

    preset: {

      guard: {
        //         weapon always first!!!
        items  : [
          'pistol',
          'heavyHelm',
          'heavyArmor',
          /* 'heavyBootBR', 'heavyBootBL'*/
        ],
        actions: {
          poseIdle : ['pose_sit', 'pose_hold_pistol'],
          actionRun: ['run', 'pose_hold_pistol'],
        },
      },

      storm: {
        items  : ['shotgun', 'lightHelm', 'lightArmor'],
        actions: {
          poseIdle : ['pose_sit', 'pose_hold_rifle'],
          actionRun: ['run', 'pose_hold_rifle'],
        },
      },

      grunt: {
        items  : ['assaultRifle', 'lightHelm', 'lightArmor'],
        actions: {
          poseIdle : ['pose_sit', 'pose_hold_rifle'],
          actionRun: ['run', 'pose_hold_rifle'],
        },
      },

      sniper: {
        items  : ['sniperRifle', 'hair', 'googles'],
        actions: {
          poseIdle : ['pose_sit', 'pose_hold_rifle'],
          actionRun: ['run', 'pose_hold_rifle'],
        },
      },
    },

    stats: {
      guard: { hp: 100, speed: 1 }
    }
  },

  camera: {
    fov     : 10,
    zoom    : 0.1,
    near    : 1,
    far     : 60000,
    aspect  : 1,
    position: [0, 100, 80],
    rotation: [-0.85, 0, 0],
  },

  maxAnisotropy: 1,
  dpr          : 1,
  fog          : {
    color: 0x000000,
    near : 0.00168,
  },

  shadow: {
    enabled      : true,
    helperEnabled: false,
    bias         : 0,
    mapWidth     : 2048,
    mapHeight    : 2048,
    near         : 250,
    far          : 400,
    top          : 100,
    right        : 100,
    bottom       : -100,
    left         : -100,
  },

  ambientLight: {
    enabled: false,
    color  : 0x7f7f7f,
  },

  directionalLight: {
    enabled  : true,
    color    : 0xffffff,
    intensity: 1,
    x        : 30,
    y        : 300,
    z        : -30,
  },

  pointLight: {
    enabled  : false,
    color    : 0xffffff,
    intensity: 0.5,
    distance : 100,
    x        : 0,
    y        : 15,
    z        : 0,
  },

  hemiLight: {
    enabled    : true,
    color      : 0xffffff,
    groundColor: 0x000000,
    intensity  : 1,
    x          : 0,
    y          : 300,
    z          : 0,
  },
}

export default Config
