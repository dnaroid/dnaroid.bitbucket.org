export const teams = [
  [
    {
      kind: 'guard',
    },
    {
      kind: 'sniper',
    },
    {
      kind: 'storm',
    },
    {
      kind: 'grunt',
    },
  ],
  [
    {
      kind: 'sniper',
    },
    {
      kind: 'guard',
    },
    {
      kind: 'storm',
    },
    {
      kind: 'grunt',
    }
  ]
]

