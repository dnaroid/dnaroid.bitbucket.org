const bone = {
  HAND: {
    bone: 'RightWrist',
  },

  HEAD: {
    bone: 'Head',
  },

  CHEST: {
    bone: 'lowerneck',
  },

  BOOT_BL: {
    bone: 'punta iz',
  },

  BOOT_BR: {
    bone: 'punta der',
  },
}

export default {
  sniperRifle : bone.HAND,
  assaultRifle: bone.HAND,
  pistol      : bone.HAND,
  shotgun     : bone.HAND,

  googles  : bone.HEAD,
  hair     : bone.HEAD,
  heavyHelm: bone.HEAD,
  lightHelm: bone.HEAD,

  heavyArmor: bone.CHEST,
  lightArmor: bone.CHEST,

  heavyBootBL: bone.BOOT_BL,
  heavyBootBR: bone.BOOT_BR,
}
