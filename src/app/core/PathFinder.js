// @flow
import * as EasyStar from 'easystarjs'
import { compilePathFor } from '../helpers/path'
import { emit, listen, on } from '../lib/Emit3r'
import { Const, Store } from '../model'
import {
  BOARD_OUT,
  BOARD_READY,
  GAME_RESUME,
  UNIT_ACCEPT_LOOK,
  UNIT_ACCEPT_PATH,
  UNIT_ADD_FIRE,
  UNIT_BRAKE_MOVE,
  UNIT_NEW_POS,
  UNIT_PLAN_MOVE
} from '../model/Event'
import type { PathStep } from '../view/Path'
// eslint-disable-next-line no-duplicate-imports
import Path from '../view/Path'
import Unit from './Unit'

const { COLOR } = Const

let grid: Array<?Array<number>>
let finder: EasyStar.js
let finderId: ?number
let plannedPath: Path

@listen
export default class PathFinder {
  constructor() {
    plannedPath = new Path(1000)
  }

  init() {
    const { board: { width, height } } = Store.resources

    grid = new Array(height)

    for (let i = 0; i < height; i++) {
      grid[i] = new Array(width).fill(0)
    }

    finder = new EasyStar.js()
    finder.setGrid(grid)
    finder.enableDiagonals()
    finder.setAcceptableTiles([0])
  }

  @on(BOARD_READY)
  fillGrid() {
    this.init()
    const { map: rows } = Store.board
    rows.forEach((col, y) => {
      col.forEach((tileId, x) => {
        grid[y][x] = tileId === 0 ? 0 : 1
      })
    })
  }

  @on(BOARD_OUT)
  hidePlannedPath() {
    plannedPath.hide()
  }

  @on(UNIT_PLAN_MOVE)
  planMove(unit: Unit, nx: number, ny: number) {
    const lastPlan = plannedPath.last
    if (lastPlan && lastPlan.x === nx && lastPlan.y === ny) {
      return
    }
    const { x, y } = unit.path.last || unit

    finder.cancelPath(finderId)
    finderId = finder.findPath(x, y, nx, ny, plan => this.onFoundPath(plan))
    finder.calculate()
    plannedPath.clear()
    plannedPath.hide()
  }

  @on(UNIT_ACCEPT_PATH)
  addPath(unit: Unit, { strafe, run }: Object) {
    if (plannedPath && plannedPath.size > 0) {
      plannedPath.steps.forEach((step: PathStep) => {
        step.strafe = strafe
        step.run    = run

        if (strafe && run) {
          step.color = COLOR.PATH.RUN_STRAFE
        } else if (run) {
          step.color = COLOR.PATH.RUN
        } else if (strafe) {
          step.color = COLOR.PATH.STRAFE
        } else {
          step.color = COLOR.PATH.MOVE
        }
      })
      unit.path.add(unit.path.size > 0 ?
                    plannedPath.steps.splice(1, plannedPath.size - 1) :
                    plannedPath.steps)
      unit.path.show()
      plannedPath.clear()
    }
  }

  @on(UNIT_ACCEPT_LOOK)
  addLook(unit: Unit, lx: number, ly: number) {
    const color    = COLOR.PATH.LOOK
    const { x, y } = unit.path.last || unit
    const look     = { x: lx, y: ly }
    const lookStep = { x, y, look, color }

    unit.path.add([lookStep])
    unit.path.show()
  }

  @on(UNIT_ADD_FIRE)
  addFire(unit: Unit, lx: number, ly: number) {
    const color    = COLOR.PATH.FIRE
    const { x, y } = unit.path.last || unit
    const fire     = { x: lx, y: ly }
    const fireStep = { x, y, fire, color }

    unit.path.add([fireStep])
    unit.path.show()
  }

  @on(GAME_RESUME)
  compilePaths() {
    Store.units.forEach((unit: Unit) =>
      compilePathFor(unit))
  }

  @on(UNIT_NEW_POS)
  checkCollision(unit: Unit) {
    const { x, y, prevX, prevY } = unit

    const checkUnit = Store.getUnitByPos(Math.round(x), Math.round(y))

    if (checkUnit && checkUnit.team !== unit.team) {
      unit.x = prevX
      unit.y = prevY
      emit(UNIT_BRAKE_MOVE, unit)
    }
  }

  onFoundPath(path: Array<PathStep>) {
    if (path !== null) {
      plannedPath.set(path)
      plannedPath.show()
    }
  }
}
