import * as THREE from 'three'
import { RESOURCES_LOADED } from '../model/Event'
import { Config, Const, Store } from '../model/index'
import log from '../helpers/logger'
import { emit } from '../lib/Emit3r'

const INIT_RES = {
  board    : {},
  cursor   : {},
  models   : {},
  sprites  : {},
  shaders  : {},
  textures : {},
  materials: {},
}

export default class Loader {
  constructor() {
    this.resources      = INIT_RES
    this.manager        = new THREE.LoadingManager()
    this._modelLoader   = new THREE.JSONLoader(this.manager)
    this._textureLoader = new THREE.TextureLoader(this.manager)
    this._fileLoader    = new THREE.FileLoader(this.manager)
    this.manager.onLoad = () => this.afterLoad()
    this.load()
  }

  load() {
    document.getElementById('loading').style.display = 'block'
    this.loadModels()
    this.loadTextures()
    this.loadSprites()
    this.loadCursor()
    this.loadBoard()
  }

  loadBoard() {
    this._fileLoader.load(
      Config.path.maps + 'level_0' + '.json',
      data => {
        const board          = JSON.parse(data)
        Store.myTeam         = 0
        board.worldWidth     = board.width * Const.SIZE.BOARD_CELL
        board.worldHeight    = board.height * Const.SIZE.BOARD_CELL
        this.resources.board = board
      }
      // #if DEBUG
      , log.progress, log.error,
      // #endif
    )
  }

  loadModels() {
    this.scanAllModels()
    Config.resources.models.forEach(name => {
      this._modelLoader.load(
        Config.path.models + name + '.json',
        (geometry, materials) => this.resources.models[name] = {
          geometry,
          materials,
        }
        // #if DEBUG
        , log.progress, log.error,
        // #endif
      )
    })
  }

  loadTextures() {
    Object.keys(Config.resources.textures).forEach(name => {
      this._textureLoader.load(
        Config.path.textures + Config.resources.textures[name] + '.jpg',
        texture => {
          texture.wrapS = texture.wrapT = THREE.RepeatWrapping
          texture.repeat.set(1, 1)
          this.resources.textures[name] = texture
        }
        // #if DEBUG
        , log.progress, log.error,
        // #endif
      )
    })
  }

  loadSprites() {
    Object.keys(Config.resources.sprites).forEach(name => {
      this._textureLoader.load(
        Config.path.sprites + Config.resources.sprites[name] + '.png',
        texture => {
          texture.wrapS = texture.wrapT = THREE.RepeatWrapping
          texture.repeat.set(1, 1)
          this.resources.sprites[name] = texture
        }
        // #if DEBUG
        , log.progress, log.error,
        // #endif
      )
    })
  }

  loadCursor() {
    const cursorNode = document.getElementById('cursor')
    Object.keys(Config.resources.cursor).forEach(name => {
      const inner     = document.createElement('div')
      inner.innerHTML = Config.resources.cursor[name]

      if (name !== 'default') {
        inner.style.display = 'none'
      }
      cursorNode.appendChild(inner)
      this.resources.cursor[name]  = inner
      this.resources.cursor.holder = cursorNode
    })
  }

  scanAllModels() {
    const models = new Set([Config.unit.modelName])
    const preset = Config.unit.preset

    Object.keys(preset).forEach(key => preset[key].items.forEach(item => models.add(item)))
    Config.resources.models.push(...models)
  }

  afterLoad() {
    document.getElementById('loading').style.display = 'none'

    Store.resources = this.resources
    emit(RESOURCES_LOADED)
  }
}
