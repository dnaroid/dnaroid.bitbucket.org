// @flow
import { UNIT_NEW_ANGLE, UNIT_NEW_POS, UNIT_STOP_MOVE } from '../model/Event'
import { emit, popMail } from '../lib/Emit3r'
import Ticker from '../lib/Ticker'
import Path from '../view/Path'
import Config from '../model/Config'

export default class Unit {
  x: number
  y: number
  id: number
  kind: string
  path: Path
  team: number
  prevX: number
  prevY: number
  angle: number
  items: Array<string>
  target: ?Unit
  isEnemy: boolean
  seeEnemy: Array<Unit>
  isVisible: boolean
  rotTicker: Ticker
  moveTicker: Ticker
  lastTargetPos: ?Object
  visibleForEnemy: boolean

  constructor(data: Object) {
    const { id, kind, team, angle, x, y, isEnemy, isVisible } = data

    this.x             = x
    this.y             = y
    this.prevX         = x
    this.prevY         = y
    this.id            = id
    this.team          = team
    this.kind          = kind
    this.path          = new Path(id)
    this.angle         = angle
    this.target        = null
    this.isEnemy       = isEnemy
    this.seeEnemy      = []
    this.isVisible     = isVisible
    this.lastTargetPos = null
    this.items         = Config.unit.preset[kind].items

    this.createTickers()
  }

  createTickers() {
    this.moveTicker = new Ticker(this)
      .onUpdate(() => {
        emit(UNIT_NEW_POS, this)
        this.prevX = this.x
        this.prevY = this.y
      })
      .onComplete(() => popMail(this.id))
      .onStop(({ x, y }) => {
        this.x = Math.round(x)
        this.y = Math.round(y)
        emit(UNIT_STOP_MOVE, this)
        emit(UNIT_NEW_POS, this)
      })

    this.rotTicker = new Ticker(this)
      .onUpdate(() =>
        emit(UNIT_NEW_ANGLE, this))
      .onComplete(({ angle }) => {
        this.angle = angle % (Math.PI * 2)
        if (!this.moveTicker.started) {
          popMail(this.id)
        }
      })
      .ease()
  }
}
