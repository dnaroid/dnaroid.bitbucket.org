// @flow
import { distance, getAngleTo } from '../helpers/math'
import { isFreeRay } from '../helpers/rayCaster'
import { clearMail, emit, listen, on, popMail } from '../lib/Emit3r'
import { Const, Store } from '../model'
import {
  BOARD_CLICK,
  BOARD_HOVER,
  BOARD_READY,
  BOARD_SELECT_CELL,
  BOARD_UNSELECT,
  CURSOR3D_SET_COLOR,
  GAME_PAUSE,
  GAME_RESUME,
  HUD_LOG,
  MOUSE_SET_CURSOR,
  SELECT_UNIT,
  SELECT_UNIT_BY_ID,
  UNIT_ACCEPT_LOOK,
  UNIT_ACCEPT_PATH,
  UNIT_ADD_FIRE,
  UNIT_BRAKE_MOVE,
  UNIT_LOOK_TO,
  UNIT_LOST_ENEMY,
  UNIT_MOVE_TO,
  UNIT_NEW_ORDER,
  UNIT_NEW_POS,
  UNIT_PLAN_MOVE,
  UNIT_ROTATE,
  UNIT_SPOT_ENEMY,
  UNITS_READY
} from '../model/Event'
import * as Mock from '../model/Mock'
import Unit from './Unit'

const { BUTTON, KEY, COLOR } = Const
let prevX                    = -1
let prevY                    = -1

@listen
export default class UnitsManager {
  constructor() {
    Store.getUnitByPos = this.getUnitByPos
    Store.setUnitByPos = this.setUnitByPos
  }

  @on(BOARD_READY)
  loadUnits() {
    Store.selectedUnit = null
    Mock.teams.forEach((team, teamIdx) =>
      team.forEach(data =>
        this.makeUnit(data, teamIdx)))

    emit(UNITS_READY)
  }

  @on(SELECT_UNIT_BY_ID)
  selectUnitById(id: number) {
    const unit: ?Unit = Store.unitById[id]
    if (unit) {
      this.selectUnit(unit)
    }
  }

  @on(UNIT_LOOK_TO)
  lookUnit(unit: Unit, x: number, y: number) {
    const xs: number    = unit.x
    const ys: number    = unit.y
    const angle: number = getAngleTo(xs, ys, x, y)

    this.rotateUnit(unit, angle)
  }

  @on(UNIT_ROTATE)
  rotateUnit(unit: Unit, angle: number) {
    const duration: number = this.prepareRotation(unit, angle)

    unit.rotTicker
      .to({ angle }, duration)
      .start()
  }

  @on(UNIT_MOVE_TO)
  moveUnit(unit: Unit, x: number, y: number, run?: boolean) {
    const duration: number = distance(unit.x, unit.y, x, y) * (run ? 5 : 20)

    unit.moveTicker
      .to({ x, y }, duration)
      .start()
  }

  @on(UNIT_BRAKE_MOVE)
  stopUnit(unit: Unit) {
    unit.moveTicker.stop()
    unit.rotTicker.stop()
  }

  @on(UNIT_NEW_ORDER)
  unitNewOrder({ id }: Unit) {
    popMail(id)
  }

  @on(GAME_PAUSE)
  onPause() {
    Store.units.forEach(unit => {
      clearMail(unit.id)
      if (unit.moveTicker.started) {
        unit.moveTicker.stop()
      }
    })

    if (Store.selectedUnit) {
      const { x, y } = Store.selectedUnit
      emit(BOARD_SELECT_CELL, x, y)
    }
  }

  @on(GAME_RESUME)
  resume() {
    emit(CURSOR3D_SET_COLOR, COLOR.CURSOR_3D.DEFAULT)
    emit(MOUSE_SET_CURSOR, COLOR.CURSOR_2D.DEFAULT)
    emit(BOARD_UNSELECT)
    Store.units.forEach(unit => popMail(unit.id))
  }

  @on(BOARD_HOVER)
  hoverBoard(x: number, y: number) {
    if (prevX === x && prevY === y) { return }
    prevX = x
    prevY = y

    const unit: ?Unit = this.getUnitByPos(x, y)
    if (unit && unit.isVisible) {
      if (unit.isEnemy && Store.selectedUnit) {
        emit(MOUSE_SET_CURSOR,
          isFreeRay(Store.selectedUnit.id, unit.id) ?
          COLOR.CURSOR_2D.TARGET : COLOR.CURSOR_2D.ENEMY)
      } else {
        emit(MOUSE_SET_CURSOR, COLOR.CURSOR_2D.ALLY)
      }
      emit(CURSOR3D_SET_COLOR, unit.isEnemy ? COLOR.CURSOR_3D.ENEMY : COLOR.CURSOR_3D.ALLY)
    }
    else {
      emit(CURSOR3D_SET_COLOR, COLOR.CURSOR_3D.DEFAULT)
      emit(MOUSE_SET_CURSOR, COLOR.CURSOR_2D.DEFAULT)
    }
    if (Store.isPaused && Store.selectedUnit) {
      this.planPath(x, y)
    }
  }

  @on(BOARD_CLICK)
  onClick(x: number, y: number, button: number) {
    const unit: ?Unit = this.getUnitByPos(x, y)

    if (unit && unit.isVisible && button === BUTTON.LEFT) {
      if (!unit.isEnemy) {
        this.selectUnit(unit)
      } else if (Store.selectedUnit) {
        this.fireTarget(unit)
      }
    } else if (Store.selectedUnit) {
      this.selectTarget(x, y, button)
    }
  }

  @on(UNIT_SPOT_ENEMY)
  showEnemy(unit: Unit) {
    unit.isVisible = true
  }

  @on(UNIT_LOST_ENEMY)
  hideEnemy(unit: Unit) {
    unit.isVisible = false
  }

  @on(UNIT_NEW_POS)
  translateUnit(unit: Unit) {
    const { x, y, prevX, prevY } = unit

    this.setUnitByPos(prevX, prevY, null)
    this.setUnitByPos(x, y, unit)
  }

  selectTarget(x: number, y: number, button: number) {
    if (button === BUTTON.LEFT) {
      const modifiers = {
        strafe: Store.keys[KEY.ALT],
        run   : Store.keys[KEY.SHIFT],
      }
      emit(UNIT_ACCEPT_PATH, Store.selectedUnit, modifiers)
    }
    if (button === BUTTON.RIGHT) {
      emit(UNIT_ACCEPT_LOOK, Store.selectedUnit, x, y)
    }
    // #if DEBUG
    if (button === BUTTON.MIDDLE) { this.lookSelectedTo(x, y) }
    // #endif
  }

  makeUnit(unitData: Object, team: number) {
    const { x, y }   = unitData
    const id         = Store.units.length
    const isEnemy    = team !== Store.myTeam
    const isVisible  = !isEnemy
    const unit: Unit = new Unit({ ...unitData, id, team, isEnemy, isVisible })

    Store.unitById[id] = unit
    Store.units.push(unit)
    Store.unitByTeam[team].push(unit)
    this.setUnitByPos(x, y, unit)
  }

  lookSelectedTo(x: number, y: number) {
    const { x: xs, y: ys } = Store.selectedUnit
    const angle: number    = getAngleTo(xs, ys, x, y)

    emit(UNIT_ROTATE, Store.selectedUnit, angle)
  }

  fireTarget(target: Unit) {
    if (isFreeRay(Store.selectedUnit.id, target.id)) {
      emit(UNIT_ADD_FIRE, Store.selectedUnit, target.x, target.y)
    } else {
      emit(HUD_LOG, 'Not line of fire')
    }
  }

  planPath(x: number, y: number) {
    emit(UNIT_PLAN_MOVE, Store.selectedUnit, x, y)
  }

  prepareRotation(unit: Unit, angle: number) {
    const delta: number = unit.angle - angle

    if (Math.abs(delta) > Math.PI) {
      unit.angle -= Math.sign(delta) * Math.PI * 2
    }
    return Math.round(Math.abs(unit.angle - angle) * 10)
  }

  selectUnit(unit: Unit) {
    if (unit.isEnemy || Store.selectedUnit === unit) {
      return
    }
    Store.selectedUnit = unit
    const { x, y }     = unit

    emit(BOARD_SELECT_CELL, x, y)
    emit(SELECT_UNIT, unit)
  }

  getUnitByPos(x: number, y: number): ?Unit {
    const column = Store.unitByPos[Math.round(x)]
    if (column) {
      return column[Math.round(y)]
    }
    return null
  }

  setUnitByPos(x: number, y: number, unit: ?Unit) {
    const rx     = Math.round(x)
    const column = Store.unitByPos[rx]

    if (!column) { Store.unitByPos[rx] = [] }

    Store.unitByPos[rx][Math.round(y)] = unit
  }
}
