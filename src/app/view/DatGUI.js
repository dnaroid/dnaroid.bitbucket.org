import * as dat from 'dat-gui'

import { Config, Store } from '../model'
import { enterFullScreen, exitFullScreen } from '../helpers/fullScreen'

const SEPARATOR = '--------------'

dat.GUI.prototype.addPosition = function (obj) {
  this.add(obj.position, 'x', -1000, 1000).name('Position X')
  this.add(obj.position, 'y', -1000, 1000).name('Position Y')
  this.add(obj.position, 'z', -1000, 1000).name('Position Z')
}

dat.GUI.prototype.addRotation = function (obj) {
  this.add(obj.rotation, 'x', -6.3, 6.3).step(0.01).name('Rotation X')
  this.add(obj.rotation, 'y', -6.3, 6.3).step(0.01).name('Rotation Y')
  this.add(obj.rotation, 'z', -6.3, 6.3).step(0.01).name('Rotation Z')
}
dat.GUI.prototype.addScale    = function (obj) {
  this.add(obj.scale, 'x').name('Scale')
    .onChange(s => obj.scale.set(s, s, s))
}

dat.GUI.prototype.addAction    = function (func, name) {
  this.add({ f: func }, 'f').name(name)
}
dat.GUI.prototype.addSeparator = function (name) {
  this.add({ [SEPARATOR]: name + SEPARATOR }, SEPARATOR)
}

dat.GUI.prototype.addThreeColor = function (obj, varName = 'color') {
  let dummy      = {}
  dummy[varName] = obj[varName].getStyle()
  return this.addColor(dummy, varName)
    .onChange(colorValue => obj[varName].setStyle(colorValue))
}

dat.GUI.prototype.addMaterial = function ({ material }) {
  this.addSeparator(material.name)
  this.addThreeColor(material)
  if (material.emissive) { this.addThreeColor(material, 'emissive') }
  if (material.emissiveIntensity) { this.add(material, 'emissiveIntensity') }
  this.add(material, 'transparent')
  this.add(material, 'opacity')
  this.add(material, 'visible')
  if (material.shininess) { this.add(material, 'shininess') }
  if (material.specular) { this.addThreeColor(material, 'specular') }
}

dat.GUI.prototype.addMaterials = function ({ materials }) {
  materials.forEach(material => { this.addMaterial(material) })
}

// Manages all dat.GUI interactions
export default class DatGUI {

  constructor() {
    const gui = new dat.GUI()

    document.getElementsByClassName('dg ac')[0].style.zIndex = 100

    /* mesh */
    const unitsFolder = gui.addFolder('Units')

    Store.units.forEach((unit, id) => {
      const unitFolder = unitsFolder.addFolder(`${unit.id} ${unit.kind}`)
      const model      = Store.modelById[id]

      /* Actions */
      const actionsFolder = unitFolder.addFolder('Actions')
      Object.keys(model.actions).forEach(name => {
        actionsFolder.addAction(() => model.playAction(name), name.replace(' ', '_'))
      })
      Object.keys(model.action).forEach(name => {
        actionsFolder.addAction(() => model.action[name](), name.replace(' ', '_'))
      })
      actionsFolder.addAction(() => model.stop(), '*stop')

      /* Materials */
      const matFolder = unitFolder.addFolder('Materials')
      model.body.material.forEach(material => {
        matFolder.addSeparator(material.name)
        matFolder.addMaterial({ material })
      })

      /* Items */
      const itemsFolder = unitFolder.addFolder('Items')
      model.body.__items.forEach(item => {
        const itemFolder = itemsFolder.addFolder(item.name)
        itemFolder.addPosition(item)
        itemFolder.addRotation(item)
        itemFolder.addScale(item)
        itemFolder.add(item, 'visible')
        itemFolder.addAction(() => {
          console.log('position', item.position)
          console.log('rotation', item.rotation)
        }, 'log')
      })
    })

    const sceneFolder = gui.addFolder('Scene')

    /* Camera */
    const camera       = Store.camera.camera
    const holder       = Store.camera.holder
    const cameraFolder = sceneFolder.addFolder('Camera')
    sceneFolder.addAction(() => enterFullScreen(), 'enter FullScreen')
    sceneFolder.addAction(() => exitFullScreen(), 'exit FullScreen')
    cameraFolder.addPosition(camera)
    cameraFolder.addRotation(camera)
    cameraFolder.addSeparator('holder')
    cameraFolder.addPosition(holder)
    cameraFolder.addRotation(holder)
    cameraFolder.add(camera, 'zoom').onFinishChange(() => {
      camera.updateProjectionMatrix()
    })
    const cameraFOVGui = cameraFolder.add(Config.camera, 'fov', 0, 180)
      .name('Camera FOV')
    cameraFOVGui.onChange((value) => { camera.fov = value })
    cameraFOVGui.onFinishChange(() => { camera.updateProjectionMatrix() })
    const cameraAspectGui = cameraFolder.add(Config.camera, 'aspect', 0, 4)
      .name('Camera Aspect')
    cameraAspectGui.onChange((value) => {
      camera.aspect = value
    })
    cameraAspectGui.onFinishChange(() => {
      camera.updateProjectionMatrix()
    })
    const cameraFogColorGui = cameraFolder.addColor(Config.fog, 'color')
      .name('Fog Color')
    cameraFogColorGui.onChange((value) => {
      Store.scene.fog.color.setHex(value)
    })
    const cameraFogNearGui = cameraFolder.add(Config.fog, 'near', 0.000, 0.010)
      .name('Fog Near')
    cameraFogNearGui.onChange((value) => {
      Store.scene.fog.density = value
    })

    /* Controls */
    // const controlsFolder = sceneFolder.addFolder('Controls')

    /* Floor */
    const floor       = Store.board.floor
    const floorFolder = gui.addFolder('Floor')
    floorFolder.addMaterial(floor)

    /* Environment */
    const envFolder = gui.addFolder('Environment')
    envFolder.addMaterial(Store.board.env.children[0])

    /* Lights */
    const light              = Store.light
    // Ambient Light
    const ambientLightFolder = sceneFolder.addFolder('Ambient Light')
    ambientLightFolder.add(Config.ambientLight, 'enabled').name('Enabled')
      .onChange((value) => {
        light.ambientLight.visible = value
      })
    ambientLightFolder.addColor(Config.ambientLight, 'color').name('Color')
      .onChange((value) => {
        light.ambientLight.color.setHex(value)
      })

    // Directional Light
    const directionalLightFolder = sceneFolder.addFolder('Directional Light')
    directionalLightFolder.add(Config.directionalLight, 'enabled')
      .name('Enabled').onChange((value) => {
      light.directionalLight.visible = value
    })
    directionalLightFolder.addColor(Config.directionalLight, 'color')
      .name('Color').onChange((value) => {
      light.directionalLight.color.setHex(value)
    })
    const directionalLightIntensityGui = directionalLightFolder
      .add(Config.directionalLight, 'intensity', 0, 2).name('Intensity')
    directionalLightIntensityGui.onChange((value) => {
      light.directionalLight.intensity = value
    })
    const directionalLightPositionXGui = directionalLightFolder
      .add(Config.directionalLight, 'x', -1000, 1000).name('Position X')
    directionalLightPositionXGui.onChange((value) => {
      light.directionalLight.position.x = value
    })
    const directionalLightPositionYGui = directionalLightFolder
      .add(Config.directionalLight, 'y', -1000, 1000).name('Position Y')
    directionalLightPositionYGui.onChange((value) => {
      light.directionalLight.position.y = value
    })
    const directionalLightPositionZGui = directionalLightFolder
      .add(Config.directionalLight, 'z', -1000, 1000).name('Position Z')
    directionalLightPositionZGui.onChange((value) => {
      light.directionalLight.position.z = value
    })

    // Shadow Map
    const shadowFolder = sceneFolder.addFolder('Shadow Map')
    shadowFolder.add(Config.shadow, 'enabled').name('Enabled').onChange((value) => {
      light.directionalLight.castShadow = value
    })
    shadowFolder.add(Config.shadow, 'helperEnabled').name('Helper Enabled').onChange((value) => {
      light.directionalLightHelper.visible = value
    })
    const shadowNearGui = shadowFolder.add(Config.shadow, 'near', 0, 400).name('Near')
    shadowNearGui.onChange((value) => {
      light.directionalLight.shadow.camera.near = value
    })
    shadowNearGui.onFinishChange(() => {
      light.directionalLight.shadow.map.dispose()
      light.directionalLight.shadow.map = null
      light.directionalLightHelper.update()
    })
    const shadowFarGui = shadowFolder.add(Config.shadow, 'far', 0, 1200).name('Far')
    shadowFarGui.onChange((value) => {
      light.directionalLight.shadow.camera.far = value
    })
    shadowFarGui.onFinishChange(() => {
      light.directionalLight.shadow.map.dispose()
      light.directionalLight.shadow.map = null
      light.directionalLightHelper.update()
    })
    const shadowTopGui = shadowFolder.add(Config.shadow, 'top', -400, 400).name('Top')
    shadowTopGui.onChange((value) => {
      light.directionalLight.shadow.camera.top = value
    })
    shadowTopGui.onFinishChange(() => {
      light.directionalLight.shadow.map.dispose()
      light.directionalLight.shadow.map = null
      light.directionalLightHelper.update()
    })
    const shadowRightGui = shadowFolder.add(Config.shadow, 'right', -400, 400).name('Right')
    shadowRightGui.onChange((value) => {
      light.directionalLight.shadow.camera.right = value
    })
    shadowRightGui.onFinishChange(() => {
      light.directionalLight.shadow.map.dispose()
      light.directionalLight.shadow.map = null
      light.directionalLightHelper.update()
    })
    const shadowBottomGui = shadowFolder.add(Config.shadow, 'bottom', -400, 400).name('Bottom')
    shadowBottomGui.onChange((value) => {
      light.directionalLight.shadow.camera.bottom = value
    })
    shadowBottomGui.onFinishChange(() => {
      light.directionalLight.shadow.map.dispose()
      light.directionalLight.shadow.map = null
      light.directionalLightHelper.update()
    })
    const shadowLeftGui = shadowFolder.add(Config.shadow, 'left', -400, 400).name('Left')
    shadowLeftGui.onChange((value) => {
      light.directionalLight.shadow.camera.left = value
    })
    shadowLeftGui.onFinishChange(() => {
      light.directionalLight.shadow.map.dispose()
      light.directionalLight.shadow.map = null
      light.directionalLightHelper.update()
    })
    const shadowBiasGui = shadowFolder.add(Config.shadow, 'bias', -0.000010, 1).name('Bias')
    shadowBiasGui.onChange((value) => {
      light.directionalLight.shadow.bias = value
    })
    shadowBiasGui.onFinishChange(() => {
      light.directionalLight.shadow.map.dispose()
      light.directionalLight.shadow.map = null
      light.directionalLightHelper.update()
    })

    // Point Light
    const pointLightFolder = sceneFolder.addFolder('Point Light')
    pointLightFolder.add(Config.pointLight, 'enabled').name('Enabled').onChange((value) => {
      light.pointLight.visible = value
    })
    pointLightFolder.addColor(Config.pointLight, 'color').name('Color').onChange((value) => {
      light.pointLight.color.setHex(value)
    })
    const pointLightIntensityGui = pointLightFolder.add(Config.pointLight, 'intensity', 0, 2).name('Intensity')
    pointLightIntensityGui.onChange((value) => {
      light.pointLight.intensity = value
    })
    const pointLightDistanceGui = pointLightFolder.add(Config.pointLight, 'distance', 0, 1000).name('Distance')
    pointLightDistanceGui.onChange((value) => {
      light.pointLight.distance = value
    })
    const pointLightPositionXGui = pointLightFolder.add(Config.pointLight, 'x', -1000, 1000).name('Position X')
    pointLightPositionXGui.onChange((value) => {
      light.pointLight.position.x = value
    })
    const pointLightPositionYGui = pointLightFolder.add(Config.pointLight, 'y', -1000, 1000).name('Position Y')
    pointLightPositionYGui.onChange((value) => {
      light.pointLight.position.y = value
    })
    const pointLightPositionZGui = pointLightFolder.add(Config.pointLight, 'z', -1000, 1000).name('Position Z')
    pointLightPositionZGui.onChange((value) => {
      light.pointLight.position.z = value
    })

    // Hemi Light
    const hemiLightFolder = sceneFolder.addFolder('Hemi Light')
    hemiLightFolder.add(Config.hemiLight, 'enabled').name('Enabled').onChange((value) => {
      light.hemiLight.visible = value
    })
    hemiLightFolder.addColor(Config.hemiLight, 'color').name('Color').onChange((value) => {
      light.hemiLight.color.setHex(value)
    })
    hemiLightFolder.addColor(Config.hemiLight, 'groundColor').name('ground Color').onChange((value) => {
      light.hemiLight.groundColor.setHex(value)
    })
    const hemiLightIntensityGui = hemiLightFolder.add(Config.hemiLight, 'intensity', 0, 2).name('Intensity')
    hemiLightIntensityGui.onChange((value) => {
      light.hemiLight.intensity = value
    })
    const hemiLightPositionXGui = hemiLightFolder.add(Config.hemiLight, 'x', -1000, 1000).name('Position X')
    hemiLightPositionXGui.onChange((value) => {
      light.hemiLight.position.x = value
    })
    const hemiLightPositionYGui = hemiLightFolder.add(Config.hemiLight, 'y', -500, 1000).name('Position Y')
    hemiLightPositionYGui.onChange((value) => {
      light.hemiLight.position.y = value
    })
    const hemiLightPositionZGui = hemiLightFolder.add(Config.hemiLight, 'z', -1000, 1000).name('Position Z')
    hemiLightPositionZGui.onChange((value) => {
      light.hemiLight.position.z = value
    })
  }
}
