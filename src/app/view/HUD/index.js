// @flow
import Unit from '../../core/Unit'
import { emit, listen, on } from '../../lib/Emit3r'
import {
  BOARD_MARK_ENEMY,
  GAME_PAUSE,
  GAME_RESUME,
  HUD_LOG,
  UNIT_SPOT_ENEMY
} from '../../model/Event'
import Console from './Console'
import HintBar from './HintBar'
import UnitsBar from './UnitsBar'

const spotted: Set<Unit> = new Set()

@listen
export default class HUD {
  console: Console

  constructor() {
    new HintBar()
    new UnitsBar()
    this.console = new Console()
  }

  @on(HUD_LOG)
  log(...args: any) {
    this.console.log(args).br()
  }

  @on(UNIT_SPOT_ENEMY)
  onSpotEnemy(enemy: Unit) {
    spotted.add(enemy)
  }

  @on(GAME_RESUME)
  resume() {
    spotted.clear()
    this.log('...resume...')
  }

  @on(GAME_PAUSE)
  pause() {
    if (spotted.size > 0) {
      this.logSpottedEnemies()
    }
    this.log('...pause...')
  }

  logSpottedEnemies() {
    this.console.log('Enemy ', 'red')

    spotted.forEach(enemy =>
      this.console.link(enemy.kind, () => emit(BOARD_MARK_ENEMY, enemy)))

    this.console.log(' spotted!', 'red').br()
  }
}
