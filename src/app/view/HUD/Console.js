const consoleNode = document.getElementById('console')

export default class Console {
  link(text, event) {
    const linkNode     = document.createElement('span')
    linkNode.innerHTML = text
    linkNode.className = 'link'
    linkNode.onclick   = event
    consoleNode.appendChild(linkNode)
    return this
  }

  log(text, colorStr = 'yellow') {
    const textNode       = document.createElement('span')
    textNode.innerHTML   = text
    textNode.style.color = colorStr
    consoleNode.appendChild(textNode)
    return this
  }

  br() {
    const textNode = document.createElement('br')
    consoleNode.appendChild(textNode)
    consoleNode.scrollTop = consoleNode.scrollHeight
    return this
  }
}
