// @flow
import { Text } from '../../model/index'
import { listen, on } from '../../lib/Emit3r'
import { GAME_PAUSE, RESOURCES_LOADED, SELECT_UNIT } from '../../model/Event'

const hintNode: any = document.getElementById('hint')

@listen
export default class HintBar {
  @on(SELECT_UNIT)
  hintUnitSelect() {
    this.showHint(Text.hint.selectUnit)
  }

  @on(GAME_PAUSE, RESOURCES_LOADED)
  pause() {
    this.hintDefault()
  }

  hintDefault() {
    this.showHint(Text.hint.default)
  }

  showHint(text: string) {
    hintNode.innerHTML = text
  }
}
