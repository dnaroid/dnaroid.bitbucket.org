// @flow

import { emit, listen, on } from '../../lib/Emit3r'
import { SELECT_UNIT, SELECT_UNIT_BY_ID } from '../../model/Event'
import { Store } from '../../model/'

@listen
export default class UnitsBar {
  nodes: Array<any>

  constructor() {
    this.nodes  = []
    const nodes = document.getElementsByClassName('unit-state')
    for (let i = 0; i < nodes.length; i++) {
      const item   = nodes.item(i)
      item.onclick = () => emit(SELECT_UNIT_BY_ID, i)
      this.nodes.push(item)
    }
  }

  @on(SELECT_UNIT)
  hintUnitSelect() {
    this.highlightSelectedUnit()
  }

  highlightSelectedUnit() {
    this.unHighlightAll()
    this.nodes[Store.selectedUnit.id].style.borderColor = '#bbb'
  }

  unHighlightAll() {
    this.nodes.forEach(node => node.style.borderColor = '#666')
  }
}
