// @flow
import * as THREE from 'three'
import { Const, Store } from '../model/index'
import { BOARD_READY, GAME_RESUME, RESOURCES_LOADED } from '../model/Event'
import * as Mock from '../model/Mock'
import { boardToWorldX, boardToWorldZ } from '../helpers/position'
import { emit, listen, on } from '../lib/Emit3r'
import Path from './Path'

const { KIND, DEG, OPACITY, SIZE, MAP: { TILES } } = Const

const VERTICAL = true

@listen
export default class Board {
  env: Object
  map: Array<Array<number>>
  floor: Object
  width: number
  height: number
  worldWidth: number
  worldHeight: number

  @on(RESOURCES_LOADED)
  init() {
    const board      = Store.resources.board
    this.width       = board.width
    this.height      = board.height
    this.worldWidth  = board.worldWidth
    this.worldHeight = board.worldHeight
    this.map         = []
    this.env         = new THREE.Group()
    this.env.kind    = 'env'

    this.parseMap(board.layers[0])
    this.makeFloor()
    this.makeBackground()
    this.makeHorizontalWalls()
    this.makeVerticalWalls()

    Store.scene.add(this.env)
    Store.board = this
    emit(BOARD_READY)
  }

  @on(GAME_RESUME)
  onResume() {
    Path.hideAll()
  }

  parseMap(layer: Object) {
    const { data, width, height } = layer
    const unitIdx                 = [0, 0]

    for (let y = 0; y < height; y++) {
      this.map[y] = []
      for (let x = 0; x < width; x++) {
        const tileId = data.splice(0, 1)[0]

        if (TILES[tileId].unit) {
          const { team, angle } = TILES[tileId].unit
          const nextUnit        = Mock.teams[team][unitIdx[team]++]
          nextUnit.x            = x
          nextUnit.y            = y
          nextUnit.angle        = angle
          this.map[y][x]        = 0
        } else {
          this.map[y][x] = tileId
        }
      }
    }
  }

  makeFloor() {
    const repeatX = this.width * 0.25
    const repeatY = this.height * 0.25
    const texture = Store.resources.textures.floor
    const normal  = Store.resources.textures.floorNormal

    texture.repeat.set(repeatX, repeatY)
    normal.repeat.set(repeatX, repeatY)

    this.floor = new THREE.Mesh(
      new THREE.PlaneBufferGeometry(this.worldWidth, this.worldHeight),
      new THREE.MeshPhongMaterial({
        map      : texture,
        color    : 0x525252,
        emissive : 0x202020,
        specular : 0x141414,
        normalMap: normal,
        shininess: 50,
      }))

    this.floor.rotateX(-DEG.D90)
    this.floor.receiveShadow = true
    this.floor.kind          = 'floor'
    this.env.add(this.floor)
    Store.scene.add(this.floor)
  }

  makeBackground() {
    const starsGeometry = new THREE.Geometry()
    for (let i = 0; i < 500; i++) {
      starsGeometry.vertices.push(new THREE.Vector3(
        1000 * Math.random() - 500,
        500 * Math.random() - 250,
        -100
      ))
    }
    const starsMaterial = new THREE.PointsMaterial()
    const stars         = new THREE.Points(starsGeometry, starsMaterial)
    stars.position.z    = -100
    Store.camera.camera.add(stars)
  }

  makeHorizontalWalls() {
    let len      = 0
    let isInside = false
    let isReady  = false
    let dx       = 0

    this.map.forEach((row, y) => {
      isInside = false
      row.forEach((cell, x) => {
        const { horizontal } = TILES[cell]

        if (horizontal) {
          isInside = true
          len += horizontal.length
          if (horizontal.start) {
            dx = -0.5
          } else if (horizontal.end) {
            dx       = 0
            isInside = false
            isReady  = true
          }
        } else if (isInside) {
          isInside = false
          isReady  = true
        }

        if (isInside && !isReady && x === this.width - 1) {
          isReady = true
          dx      = 0.5
        }

        if (isReady) {
          this.makeWall(x + dx, y, len)
          isReady = false
          len     = 0
          dx      = 0
        }
      })
    })
  }

  makeVerticalWalls() {
    let len      = 0
    let isInside = false
    let isReady  = false
    let dy       = 0

    for (let x = 0; x < this.width; x++) {
      isInside = false
      for (let y = 0; y < this.height; y++) {
        const { vertical } = TILES[this.map[y][x]]

        if (vertical) {
          isInside = true
          len += vertical.length
          if (vertical.start) {
            dy = -0.5
          } else if (vertical.end) {
            dy       = 0
            isInside = false
            isReady  = true
          }
        } else if (isInside) {
          isInside = false
          isReady  = true
        }

        if (isInside && !isReady && y === this.height - 1) {
          isReady = true
          dy      = 0.5
        }

        if (isReady) {
          this.makeWall(x, y + dy, len, VERTICAL)
          isReady = false
          len     = 0
          dy      = 0
        }
      }
    }
  }

  makeWall(x: number, y: number, len: number, isVertical: boolean = false) {
    const texture = Store.resources.textures.wall
    const normal  = Store.resources.textures.wallNormal

    const material = new THREE.MeshPhongMaterial({
      map      : texture,
      color    : '#ffcdcd',
      opacity  : OPACITY.WALL,
      specular : '#7f7f7f',
      shininess: 100,
      normalMap: normal,
    })

    const REPEAT_X = 1.5
    const REPEAT_Y = 4
    texture.repeat.set(REPEAT_X, REPEAT_Y)
    normal.repeat.set(REPEAT_X, REPEAT_Y)

    const geometry = new THREE.BoxBufferGeometry(
      SIZE.BOARD_CELL * len + (isVertical ? SIZE.WALL_THICKNESS : -SIZE.WALL_THICKNESS),
      SIZE.WALL_HEIGHT,
      SIZE.WALL_THICKNESS)

    const wall      = new THREE.Mesh(geometry, material)
    wall.kind       = KIND.WALL
    const boardX    = x - (isVertical ? 0 : len / 2)
    const boardY    = y - (isVertical ? len / 2 : 0)
    wall.castShadow = true

    wall.rotateY(isVertical ? DEG.D90 : 0)
    wall.position.set(boardToWorldX(boardX), SIZE.WALL_HEIGHT / 2, boardToWorldZ(boardY))
    this.env.add(wall)
  }
}
