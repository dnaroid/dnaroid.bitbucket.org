// @flow
import { Const, Store } from '../model'
import Config from '../model/Config'
import {
  BOARD_READY,
  UNIT_LOST_ENEMY,
  UNIT_NEW_POS,
  UNIT_SPOT_ENEMY,
  UNITS_READY
} from '../model/Event'
import Unit from '../core/Unit'
import { listen, on } from '../lib/Emit3r'

const { MAP } = Const

const SCALE = 3

const WALL  = 1
const ALLY  = 2
const ENEMY = 3
const EMPTY = 0

const COLOR  = []
COLOR[WALL]  = [255, 255, 255]
COLOR[ALLY]  = [0, 255, 0]
COLOR[ENEMY] = [255, 0, 0]
COLOR[EMPTY] = [0, 0, 0]

const mapCSS = {
  right     : '0',
  top       : '0',
  zIndex    : '5',
  width     : `${Config.HUD.map.width}px`,
  height    : `${Config.HUD.map.height}px`,
  position  : 'absolute',
  background: 'black',
}

let map: Uint8ClampedArray
let ctx: CanvasRenderingContext2D
let width: number  = 0
let height: number = 0
let canvas: HTMLCanvasElement
let imgData: ImageData

const offset = (x, y) => (Math.round(y) * width + Math.round(x)) * 4

let isNeedRefresh: boolean = false

@listen
export default class MiniMap {

  static update() {
    if (isNeedRefresh) {
      ctx.putImageData(imgData, 0, 0)
      isNeedRefresh = false
    }
  }

  init() {
    const { board } = Store.resources
    this.makeNode()
    width   = board.width * SCALE
    height  = board.height * SCALE
    imgData = ctx.createImageData(width, height)
    map     = imgData.data.fill(0)

    for (let i = 3; i < map.length; i = i + 4) {
      map[i] = 127
    }
    canvas.width  = width
    canvas.height = height
  }

  @on(UNITS_READY)
  drawUnits() {
    Store.units.forEach(unit => {
      const { x, y, isEnemy } = unit

      if (!isEnemy) {
        this.drawPixel(x * SCALE + 1, y * SCALE + 1, ALLY)
      }
    })
  }

  @on(BOARD_READY)
  drawWalls() {
    this.init()
    const { map: rows } = Store.board
    rows.forEach((col, y) => {
      col.forEach((cell, x) => {
        this.drawTile(x, y, cell)
      })
    })
  }

  drawTile(x: number, y: number, char: string) {
    const pixels = MAP.PIXELS[char] || []
    for (let i = 0; i < pixels.length; i += 2) {
      this.drawPixel(x * 3 + pixels[i] + 1, y * 3 + pixels[i + 1] + 1, WALL)
    }
  }

  @on(UNIT_NEW_POS)
  translateUnit(unit: Unit) {
    const { x, y, isEnemy, prevX, prevY } = unit

    this.drawPixel(prevX * SCALE + 1, prevY * SCALE + 1, EMPTY)
    if (unit.isVisible) {
      this.drawPixel(x * SCALE + 1, y * SCALE + 1, isEnemy ? ENEMY : ALLY)
    }
  }

  @on(UNIT_SPOT_ENEMY)
  showEnemy({ x, y }: Unit) {
    this.drawPixel(x * SCALE + 1, y * SCALE + 1, ENEMY)
  }

  @on(UNIT_LOST_ENEMY)
  hideEnemy({ x, y }: Unit) {
    this.drawPixel(x * SCALE + 1, y * SCALE + 1, EMPTY)
  }

  makeNode() {
    if (canvas === undefined) {
      canvas    = document.createElement('canvas')
      canvas.id = 'map'

      // $FlowFixMe
      Object.keys(mapCSS).forEach(key => (canvas.style[key] = mapCSS[key]))
      // $FlowFixMe
      document.body.appendChild(canvas)
      // $FlowFixMe
      ctx = canvas.getContext('2d')
    }
  }

  drawPixel(x: number, y: number, color: number) {
    map.set(COLOR[color], offset(x, y))
    isNeedRefresh = true
  }
}
