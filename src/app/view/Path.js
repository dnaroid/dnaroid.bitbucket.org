// @flow
import * as THREE from 'three'
import { Const, Store } from '../model/index'
import { boardToWorldX, boardToWorldZ } from '../helpers/position'
import { getAngleTo } from '../helpers/math'

const { POSITION, COLOR, SIZE } = Const

const POINT_RADIUS = 1
const ARROW_RADIUS = 1

export type PathStep = {
  x: number,
  y: number,
  dx?: number,
  dy?: number,
  color: string,
  strafe?: boolean,
  run?: boolean,
  look?: Object,
  fire?: Object,
  del?: boolean
}

const pool = []

export default class Path {
  id: number
  _steps: Array<PathStep>

  constructor(id: number) {
    this.id     = id
    this._steps = []
  }

  static hideAll() {
    pool.forEach(el => {
      el.__free  = true
      el.visible = false
    })
  }

  get size(): number {
    return this._steps.length
  }

  get first(): PathStep {
    return this._steps[0]
  }

  get last(): PathStep {
    return this._steps[this._steps.length - 1]
  }

  get steps(): Array<PathStep> {
    return this._steps
  }

  set(steps: Array<PathStep>) {
    this._steps = steps
  }

  clear() {
    this._steps.length = 0
    this.hide()
  }

  add(steps: Array<PathStep>) {
    const newSteps = Array.isArray(steps) ? steps : [steps]
    this._steps    = [...this._steps, ...newSteps]
  }

  show() {
    this.hide()

    if (this.size < 1) { return}

    let { x: prevX, y: prevY } = this.first

    for (let i = 1; i < this.size; i++) {
      const { x, y, color, look, fire } = this._steps[i]
      if (look || fire) {
        const target: Object = { ...look, ...fire }
        this._addArrow(x, y, target.x, target.y, color)
        this._addPoint(x, y, color)
      } else {
        this._addArrow(prevX, prevY, x, y, color)
      }
      prevX = x
      prevY = y
    }
  }

  hide() {
    pool.forEach(el => {
      if (el.__id === this.id) {
        el.__free  = true
        el.visible = false
      }
    })
  }

  _addPoint(x: number, y: number, color: string = COLOR.PATH.PREVIEW) {
    const wx1 = boardToWorldX(x)
    const wz1 = boardToWorldZ(y)
    let point = null

    for (let i = 0; i < pool.length; i++) {
      const mesh = pool[i]
      if (mesh.__free && mesh.__point) {
        point = mesh
        break
      }
    }
    point = point || this._newPoint()

    point.position.set(wx1, POSITION.PATH_Y, wz1)
    point.material.color.set(color)
    point.__free  = false
    point.__id    = this.id
    point.visible = true
    return point
  }

  _addArrow(x1: number, y1: number, x2: number, y2: number, color: string = COLOR.PATH.PREVIEW): Object {
    const wx1   = boardToWorldX(x1)
    const wz1   = boardToWorldZ(y1)
    const angle = getAngleTo(x1, y1, x2, y2)

    let arrow = null
    for (let i = 0; i < pool.length; i++) {
      const mesh = pool[i]
      if (mesh.__free && mesh.__arrow) {
        arrow = mesh
        break
      }
    }
    arrow = arrow || this._newArrow()

    arrow.position.set(wx1, POSITION.PATH_Y, wz1)
    arrow.rotation.y = angle
    arrow.material.color.set(color)
    arrow.__free  = false
    arrow.__id    = this.id
    arrow.visible = true
    return arrow
  }

  _newPoint(): Object {
    const geometry = new THREE.CylinderBufferGeometry(POINT_RADIUS, POINT_RADIUS, 0.1, 64)
    const material = new THREE.LineBasicMaterial()

    const point   = new THREE.Mesh(geometry, material)
    point.__id    = this.id
    point.__point = true

    pool.push(point)
    Store.scene.add(point)
    return point
  }

  _newArrow(): Object {
    const lineGeometry  = new THREE.BoxBufferGeometry(SIZE.PATH_WIDTH, 0.1, SIZE.PATH_LENGTH)
    const arrowGeometry = new THREE.CylinderBufferGeometry(ARROW_RADIUS, ARROW_RADIUS, 0.1, 3)
    const material      = new THREE.MeshBasicMaterial({
      transparent: true,
      opacity    : 0.6
    })
    const line          = new THREE.Mesh(lineGeometry, material)
    const triangle      = new THREE.Mesh(arrowGeometry, material)
    triangle.position.z = 7
    line.position.z     = 4

    const arrow = new THREE.Group()
    arrow.add(line)
    arrow.add(triangle)
    arrow.material = material

    arrow.__id    = this.id
    arrow.__arrow = true
    pool.push(arrow)
    Store.scene.add(arrow)
    return arrow
  }

  compress() {
    if (this.size === 0) {return}
    const end = this.size

    for (let i = 1; i < end; i++) {
      const current = this._steps[i]
      const prev    = this._steps[i - 1]

      current.dx = Math.sign(current.x - prev.x)
      current.dy = Math.sign(current.y - prev.y)
      prev.del   = !current.look &&
                   !current.fire &&
                   current.dx === prev.dx &&
                   current.dy === prev.dy &&
                   current.strafe === prev.strafe &&
                   current.run === prev.run
    }
    this._steps = this._steps.filter(step => !step.del)
  }
}
