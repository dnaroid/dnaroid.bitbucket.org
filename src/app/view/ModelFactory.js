// @flow
import * as THREE from 'three'
import { Attach, Config, Const, Store } from '../model/index'
import {
  BOARD_READY,
  GAME_PAUSE,
  MODELS_READY,
  UNIT_LOST_ENEMY,
  UNIT_NEW_ANGLE,
  UNIT_NEW_POS,
  UNIT_SPOT_ENEMY,
  UNIT_START_MOVE,
  UNIT_STOP_MOVE,
  UNITS_READY
} from '../model/Event'
import Unit from '../core/Unit'
import { getActiveTracks } from '../helpers/action'
import { boardToWorldX, boardToWorldZ } from '../helpers/position'
import { emit, listen, on } from '../lib/Emit3r'

const { COLOR } = Const

@listen
export default class ModelFactory {

  @on(UNIT_SPOT_ENEMY)
  showModel({ id }: Unit) {
    Store.bodyById[id].visible = true
  }

  @on(UNIT_LOST_ENEMY)
  hideModel({ id }: Unit) {
    Store.bodyById[id].visible = false
  }

  @on(UNIT_NEW_POS)
  updatePosition({ id, x, y }: Unit) {
    Store.bodyById[id].position.x = boardToWorldX(x)
    Store.bodyById[id].position.z = boardToWorldZ(y)
  }

  @on(UNIT_NEW_ANGLE)
  updateRotation({ id, angle }: Unit) {
    Store.bodyById[id].rotation.y = angle
  }

  @on(UNIT_STOP_MOVE)
  stopMove({ id }: Unit) {
    Store.modelById[id].action.poseIdle()
  }

  @on(GAME_PAUSE)
  stopMoveAll() {
    Object.values(Store.modelById).forEach((model: any) =>
      model.action.poseIdle())
  }

  @on(UNIT_START_MOVE)
  startMove({ id }: Unit) {
    Store.modelById[id].action.actionRun()
  }

  @on([UNITS_READY, BOARD_READY])
  makeModels() {
    Store.units.forEach(unit => {
      const { x, y, angle, id, isVisible } = unit

      const body    = this.makeBody(unit)
      const actions = this.makeActions(body)
      const action  = this.makeMixedActions(body)

      body.visible = isVisible
      body.position.set(boardToWorldX(x), 0, boardToWorldZ(y))
      body.rotation.set(0, angle, 0)

      Store.modelById[id] = { body, action, actions }
      Store.bodyById[id]  = body
      Store.bodies.push(body)
      Store.modelById[id].action.poseIdle()
    })
    emit(MODELS_READY)
  }

  playAction(id: number, name: string) {
    const { actions } = Store.modelById[id]

    Object.keys(actions).forEach(n => {
      if (n === name) {
        actions[n].play()
      }
      else {
        actions[n].stop()
      }
    })
  }

  playActions(id: number, names: Array<string>) {
    const { actions } = Store.modelById[id]

    Object.keys(actions).forEach(name => {
      if (names.includes(name)) {
        actions[name].play()
      }
      else {
        actions[name].stop()
      }
    })
  }

  makeBody(unit: Unit) {
    const { kind, team, id } = unit

    const { modelName }           = Config.unit
    const { geometry, materials } = Store.resources.models[modelName]

    const bodyMaterials = this.parsedMaterials(materials, team, true)
    const body          = new THREE.SkinnedMesh(geometry, bodyMaterials)
    body.__mixer        = new THREE.AnimationMixer(body)

    const size   = body.geometry.boundingSphere.radius
    const target = new THREE.Mesh(
      new THREE.BoxBufferGeometry(size / 2, size * 1.5, size / 2),
      new THREE.MeshBasicMaterial({
        color      : '#ff0000',
        opacity    : 0,
        transparent: true,
      }))

    target.position.y = size / 1.5
    target.__unitId   = id

    body.__kind     = kind
    body.__target   = target
    body.__team     = team
    body.__unitId   = id
    body.__items    = []
    body.castShadow = true

    body.add(target)

    Store.mixers.push(body.__mixer)
    Store.scene.add(body)
    this.makeItems(body)

    return body
  }

  makeActions(body: Object) {
    const { geometry: { animations }, __mixer } = body

    const actions = {}

    if (animations) {
      animations.forEach(animation => {
        const { name, uuid, duration } = animation

        const tracks = [...getActiveTracks(animation.tracks)]
        const action = __mixer.clipAction({ duration, name, tracks, uuid })

        if (name.startsWith('pose')) {
          action.setLoop(THREE.LoopOnce, 0)
          action.clampWhenFinished = true
        } else {
          // todo fix in model
          action.timeScale = 3
        }
        actions[name] = action
      })
    }

    return actions
  }

  makeMixedActions(body: Object) {
    const { __unitId, __kind } = body
    const { actions }          = Config.unit.preset[__kind]
    const mixActions           = {}

    if (actions) {
      const names = Object.keys(actions)

      names.forEach(mixName =>
        mixActions[mixName] = () => this.playActions(__unitId, actions[mixName]))
    }
    return mixActions
  }

  makeItems(body: Object) {
    const { __kind } = body
    const { items }  = Config.unit.preset[__kind]

    items.forEach(name =>
      this.makeItem(body, name))
  }

  makeItem(body: Object, name: string) {
    const { __team }              = body
    const { geometry, materials } = Store.resources.models[name]
    const itemMaterials           = this.parsedMaterials(materials, __team, true)

    const item      = new THREE.Mesh(geometry, itemMaterials)
    item.name       = name
    body[name]      = item
    item.unitId     = body.unitId
    item.castShadow = true
    this.attachItem(body, item)
  }

  attachItem(body: Object, item) {
    const options = Attach[item.name]
    const name    = options.bone
    const bone    = this.getBoneByName(body, name)

    if (bone) {
      bone.add(item)
      body.__items.push(item)
    }
    // #if DEBUG
    else { console.error(`Bone '${name}' not found!`) }
    // #endif
  }

  getBoneByName(body: Object, name: string) {
    return body.skeleton.bones
      .find(bone => bone.name === name)
  }

  parsedMaterials(materials: Array<Object> = [], team: number, skinning: boolean = false) {
    const result = []
    materials.forEach(material => {
      const { name }    = material
      material.skinning = skinning
      if (name === 'TEAM') {
        const clone = material.clone()
        clone.color.set(COLOR.TEAM[team])
        result.push(clone)
      } else {
        result.push(material)
      }
    })
    return result
  }
}
