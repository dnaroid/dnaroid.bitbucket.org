import HeightMap from '../MiniMap'
//https://www.npmjs.com/package/canvas

const BLACK = 0
const WALL  = 1
const ALLY  = 2
const ENEMY = 3

const hmap   = new HeightMap()
const WIDTH  = 5
const HEIGHT = 5

beforeEach(() => {
  hmap.init(WIDTH, HEIGHT)
})

describe('MiniMap 5x5', () => {
  it('create map', () => {
    expect(hmap.map).toMatchSnapshot()
  })

  it('drawPixel at 0,0', () => {
    const x = 0
    const y = 0
    hmap.drawPixel(x, y, WALL)
    expect(hmap.map).toMatchSnapshot()
  })

  it('drawPixel at 2,0', () => {
    const x = 2
    const y = 0
    hmap.drawPixel(x, y, WALL)
    expect(hmap.map).toMatchSnapshot()
  })

  it('drawPixel at 0,1', () => {
    const x = 0
    const y = 1
    hmap.drawPixel(x, y, WALL)
    expect(hmap.map).toMatchSnapshot()
  })
})


