// @flow
import { Store } from '../model/index'
import {
  BOARD_READY,
  MODELS_READY,
  UNIT_LOST_ALLY,
  UNIT_LOST_ENEMY,
  UNIT_NEW_ANGLE,
  UNIT_NEW_POS,
  UNIT_SPOT_ALLY,
  UNIT_SPOT_ENEMY,
} from '../model/Event'
import { isInFOV } from '../helpers/math'
import { isFreeRay } from '../helpers/rayCaster'
import { emit, listen, on } from '../lib/Emit3r'
import Unit from '../core/Unit'

let enemyIds: Array<number> = []
let allyIds: Array<number>  = []
const visibleEnemy          = new Set()
const visibleAlly           = new Set()

@listen
export default class FOV {

  @on([MODELS_READY, BOARD_READY])
  init() {
    enemyIds = []
    allyIds  = []
    Store.units.forEach(unit => {
      if (unit.isEnemy) {
        unit.visible = false
        enemyIds.push(unit.id)
      } else {
        unit.visibleForEnemy = false
        allyIds.push(unit.id)
      }
    })
    //this.updateUnitsSees()
  }

  @on(UNIT_NEW_ANGLE,
    UNIT_NEW_POS)
  updateUnitsSees() {
    Store.visiblity = []
    visibleEnemy.clear()
    visibleAlly.clear()
    Store.units.forEach((me: Unit) => {
      Store.visiblity[me.id] = []
      me.seeEnemy            = []
      Store.units.forEach((other: Unit) => {
        if (me.id !== other.id && me.team !== other.team && isInFOV(me, other)) {
          if (isFreeRay(me.id, other.id)) {
            Store.visiblity[me.id].push(other.id)
            me.seeEnemy.push(other)
            if (me.isEnemy) {
              visibleAlly.add(other.id)
            } else {
              visibleEnemy.add(other.id)
            }
          }
        }
      })
    })
    this.updateEnemiesVisible()
    this.updateAlliesVisible()
  }

  updateEnemiesVisible() {
    enemyIds.forEach(id => {
      const enemy: Unit = Store.unitById[id]
      if (visibleEnemy.has(id)) {
        if (!enemy.isVisible) {
          emit(UNIT_SPOT_ENEMY, enemy)
        }
      } else if (enemy.isVisible) {
        emit(UNIT_LOST_ENEMY, enemy)
      }
    })
  }

  updateAlliesVisible() {
    allyIds.forEach(id => {
      const ally: Unit = Store.unitById[id]
      if (visibleAlly.has(id)) {
        if (!ally.visibleForEnemy) {
          ally.visibleForEnemy = true
          emit(UNIT_SPOT_ALLY, ally)
        }
      } else if (ally.visibleForEnemy) {
        ally.visibleForEnemy = false
        emit(UNIT_LOST_ALLY, ally)
      }
    })
  }
}
