// @flow
import { UNIT_LOOK_TO, UNIT_LOST_ALLY, UNIT_NEW_POS, UNIT_SPOT_ALLY, UNITS_READY } from '../model/Event'
import { Store } from '../model/index'
import Unit from '../core/Unit'
import { emit, listen, on } from '../lib/Emit3r'

const targetByUnit: WeakMap<Unit, ?Unit>         = new WeakMap()
const unitsByTarget: WeakMap<Unit, ?Array<Unit>> = new WeakMap()
let AIUnits: Array<Unit>

@listen
export default class AI {

  @on(UNITS_READY)
  takeAIUnits() {
    AIUnits = Store.units.filter(unit => unit.isEnemy)
  }

  @on(UNIT_SPOT_ALLY)
  spotTarget(target: Unit) {
    const activeUnits = this.getUnitsIdsSeeTarget(target)
    if (activeUnits.length > 0) {
      activeUnits.forEach(unit => this.setTarget(unit, target))
      this.lookTarget(target)
    }
  }

  @on(UNIT_NEW_POS)
  lookTarget(target: Unit) {
    const units = unitsByTarget.get(target)
    if (units) {
      units.forEach((unit: Unit) => {
        if (!unit.rotTicker.started) {
          emit(UNIT_LOOK_TO, unit, target.x, target.y)
        }
      })
    }
  }

  @on(UNIT_LOST_ALLY)
  resetTarget(target: Unit) {
    const unitsWithThisTarget = unitsByTarget.get(target)
    if (unitsWithThisTarget) {
      unitsWithThisTarget.forEach((unit: Unit) => {
        unit.lastTargetPos = { x: target.x, y: target.y }
        unit.target        = null
        targetByUnit.set(unit, null)
        this.searchTarget(unit)
      })
    }
  }

  setTarget(me: Unit, target: Unit) {
    if (!me.target) {
      me.target = target
      targetByUnit.set(target, me)
      unitsByTarget.set(target, [...unitsByTarget.get(target) || [], me])
    }
  }

  // todo fix cheat
  searchTarget(me: Unit) {
    if (me.lastTargetPos) {
      const { x, y } = me.lastTargetPos
      emit(UNIT_LOOK_TO, me, x, y)
    }
  }

  getUnitsIdsSeeTarget(target: Unit): Array<Unit> {
    return AIUnits.filter(unit => unit.seeEnemy.includes(target))
  }
}
