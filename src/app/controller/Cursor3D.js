// @flow
import * as THREE from 'three'
import Unit from '../core/Unit'
import {
  boardToWorldX,
  boardToWorldZ,
  cursorToBoardX,
  cursorToBoardY
} from '../helpers/position'
import { emit, listen, on } from '../lib/Emit3r'
import Ticker from '../lib/Ticker'

import { Const, Store } from '../model'
import {
  BOARD_CLICK,
  BOARD_HOVER,
  BOARD_MARK_ENEMY,
  BOARD_OUT,
  BOARD_READY,
  BOARD_SELECT_CELL,
  BOARD_UNSELECT,
  CAMERA_MOVE,
  CAMERA_ROTATE_X,
  CAMERA_ROTATE_Y,
  CAMERA_SET_FLOOR_POS,
  CURSOR3D_HIDE,
  CURSOR3D_SET_COLOR,
  CURSOR3D_SHOW,
  GAME_RESUME,
  MODELS_READY,
  MOUSE_CLICK,
  MOUSE_HOVER,
  UNIT_NEW_ANGLE,
  UNIT_NEW_POS
} from '../model/Event'

const { COLOR, OPACITY, SIZE } = Const

const direction: THREE.Vector3          = new THREE.Vector3()
const raycaster: THREE.Raycaster        = new THREE.Raycaster()
const shadedByCursor: Array<THREE.Mesh> = []
const shadedByUnit: Array<THREE.Mesh>   = []
const MESH_MATERIAL                     = {
  color      : COLOR.CURSOR_3D.SELECTED,
  transparent: true,
  opacity    : OPACITY.CURSOR_3D,
}

@listen
export default class Cursor3D {
  x: number
  y: number
  isHover: boolean
  cursor: THREE.Mesh
  selectMark: THREE.Mesh
  enemyMark: THREE.Mesh

  @on(BOARD_READY)
  init() {
    this.x = -1
    this.y = -1

    this.makeMeshes()
    this.hide()
    this.unSelectCell()
    Store.cursor = this
  }

  @on(CURSOR3D_SET_COLOR)
  setColor(color: string) {
    this.cursor.material.color.set(color)
  }

  @on(CURSOR3D_SHOW)
  show(value: boolean = true) {
    this.cursor.material.opacity = value ? OPACITY.CURSOR_3D : 0
  }

  @on(CURSOR3D_HIDE)
  hide() {
    this.cursor.material.opacity = 0
  }

  @on(BOARD_SELECT_CELL)
  selectCell(x: number, y: number) {
    this.selectMark.position.x       = boardToWorldX(x)
    this.selectMark.position.z       = boardToWorldZ(y)
    this.selectMark.position.y       = 0
    this.selectMark.material.opacity = OPACITY.CURSOR_3D
  }

  @on(BOARD_UNSELECT)
  unSelectCell() {
    this.selectMark.position.y       = -1
    this.selectMark.material.opacity = 0
  }

  @on(MOUSE_HOVER)
  updatePos(x: number, y: number) {
    this.shadeByCursor(x, y)
    raycaster.setFromCamera({ x, y }, Store.camera.camera)
    const [floor] = raycaster.intersectObject(Store.board.floor, false)

    if (floor) {
      const { x: fx, z: fz } = floor.point
      this.x                 = cursorToBoardX(fx)
      this.y                 = cursorToBoardY(fz)
      this.cursor.position.x = boardToWorldX(this.x)
      this.cursor.position.z = boardToWorldZ(this.y)

      if (Store.isPaused) {
        emit(BOARD_HOVER, this.x, this.y)
        this.isHover = true
      }
      emit(CAMERA_SET_FLOOR_POS, fx, fz)
      this.show()
    } else {
      this.x = -1
      this.y = -1
      this.hide()
      this.isHover = false
      emit(BOARD_OUT)
    }
  }

  @on(MOUSE_CLICK)
  sendClick(x: number, y: number, button: number) {
    if (Store.isPaused && this.x >= 0 && this.y >= 0) {
      emit(BOARD_CLICK, this.x, this.y, button)
    }
  }

  @on(
    [MODELS_READY, BOARD_READY],
    CAMERA_MOVE,
    CAMERA_ROTATE_X,
    CAMERA_ROTATE_Y,
    UNIT_NEW_ANGLE,
    UNIT_NEW_POS)
  shadeByUnit() {
    this.unShadeAllByUnit()
    const camPos: THREE.Vector3 = Store.camera.camera.getWorldPosition()

    const bodies = [...Store.bodies, this.enemyMark]

    bodies.forEach(body => {
      if (body.visible) {
        const unitPos: THREE.Vector3 = body.getWorldPosition()
        const angle: THREE.Vector3   = direction.subVectors(unitPos, camPos).normalize()
        raycaster.set(camPos, angle)

        raycaster
          .intersectObjects([...Store.board.env.children], false)
          .forEach(({ object }) =>
            this.shadeMeshByUnit(object))
      }
    })
  }

  @on(BOARD_MARK_ENEMY)
  markEnemy(enemy: Unit) {
    this.enemyMark.position.x = boardToWorldX(enemy.x)
    this.enemyMark.position.z = boardToWorldZ(enemy.y)
    this.enemyMark.visible    = true
    this.shadeByUnit()
  }

  @on(GAME_RESUME)
  unmarkEnemy() {
    this.enemyMark.visible = false
    this.shadeByUnit()
  }

  shadeByCursor(x: number, y: number) {
    this.unShadeAllByCursor()
    raycaster.setFromCamera({ x, y }, Store.camera.camera)

    raycaster
      .intersectObjects(Store.board.env.children, false)
      .forEach(({ object }) =>
        this.shadeMeshByCursor(object))
  }

  unShadeAllByUnit() {
    shadedByUnit.forEach((obj, idx) => {
      obj.material.transparent = false
      shadedByUnit.splice(idx, 1)
    })
  }

  unShadeAllByCursor() {
    shadedByCursor.forEach((obj, idx) => {
      if (!shadedByUnit.includes(obj)) {
        obj.material.transparent = false
        shadedByCursor.splice(idx, 1)
      }
    })
  }

  shadeMeshByCursor(obj: THREE.Mesh) {
    if (!shadedByUnit.includes(obj)) {
      shadedByCursor.push(obj)
      obj.material.transparent = true
    }
  }

  shadeMeshByUnit(obj: THREE.Mesh) {
    shadedByUnit.push(obj)
    obj.material.transparent = true
  }

  makeMeshes() {
    this.cursor = new THREE.Mesh(
      new THREE.BoxBufferGeometry(SIZE.BOARD_CELL, 0.1, SIZE.BOARD_CELL),
      new THREE.MeshBasicMaterial(MESH_MATERIAL))

    Store.scene.add(this.cursor)

    this.selectMark = new THREE.Mesh(
      new THREE.BoxBufferGeometry(SIZE.BOARD_CELL, 0.2, SIZE.BOARD_CELL),
      new THREE.MeshBasicMaterial(MESH_MATERIAL))

    new Ticker(this.selectMark.material)
      .to({ opacity: OPACITY.BOARD_CELL_MIN }, 20)
      .repeat().yoyo().start()

    Store.scene.add(this.selectMark)

    this.enemyMark = new THREE.Mesh(
      new THREE.CylinderBufferGeometry(SIZE.BOARD_CELL * 0.3, SIZE.BOARD_CELL * 0.3, 0.2, 64),
      new THREE.MeshBasicMaterial({
        color      : COLOR.CURSOR_3D.ENEMY,
        opacity    : 0.7,
        transparent: true,
      }))

    new Ticker(this.enemyMark.material)
      .to({ opacity: OPACITY.BOARD_CELL_MIN }, 10)
      .repeat().yoyo().start()

    this.enemyMark.visible = false
    Store.scene.add(this.enemyMark)
  }
}
