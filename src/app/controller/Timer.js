import { Store, Config } from '../model/index'
import { formatTime } from '../helpers/time'
import {
  GAME_PAUSE,
  RESOURCES_LOADED,
  GAME_RESUME,
  START_NEXT_TURN
} from '../model/Event'
import { emit, listen, on } from '../lib/Emit3r'
import { PAUSE, PLAY } from '../helpers/svg'

const timerNode    = document.getElementById('timer')
const controlsNode = document.getElementById('controls')

@listen
export default class Timer {
  constructor() {
    Store.time = 0
  }

  static update(delta) {
    Store.time += delta
    timerNode.innerHTML = formatTime(Store.time)
    if ((Store.time - Store.prevTime) > Config.tourTime) {
      Store.isPaused         = true
      Store.prevTime         = Store.time
      controlsNode.innerHTML = PLAY
      emit(GAME_PAUSE)
    }
  }

  @on(RESOURCES_LOADED)
  init() {
    controlsNode.innerHTML = PLAY
    controlsNode.onclick   = () => this.startTurn()
  }

  @on(START_NEXT_TURN)
  startTurn() {
    if (Store.isPaused) {
      Store.isPaused         = false
      controlsNode.innerHTML = PAUSE
      Store.prevTime         = Store.time
      emit(GAME_RESUME)
    }
  }
}
