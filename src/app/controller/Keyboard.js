// @flow
import { Const, Store } from '../model'
import { SELECT_UNIT_BY_ID, START_NEXT_TURN } from '../model/Event'
import { emit, listen, on } from '../lib/Emit3r'
import DatGUI from '../view/datGUI'

let debugDatGUI: boolean = false

@listen
export default class Keyboard {
  constructor() {
    Object.values(Const.KEY).forEach(code => (Store.keys[code] = 0))
  }

  @on(window, 'keydown')
  keyDown({ keyCode }: { keyCode: number }) {
    Store.keys[keyCode] = 1
    this.parseKey(keyCode)
  }

  @on(window, 'keyup')
  keyUp({ keyCode }: { keyCode: number }) {
    Store.keys[keyCode] = 0
  }

  parseKey(code: number) {
    //console.log('keyCode', code)
    if (code === 32 && Store.isPaused) { // [space]
      emit(START_NEXT_TURN)
    }

    if (code > 48 && code < 53) {  // [1]...[4]
      emit(SELECT_UNIT_BY_ID, code - 49)
    }

    // #if DEBUG
    if (code === 68 && !debugDatGUI) { // [D]
      debugDatGUI = true
      new DatGUI()
    }
    // #endif
  }
}
