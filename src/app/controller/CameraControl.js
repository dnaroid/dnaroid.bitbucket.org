// @flow
import {
  CAMERA_MOVE,
  CAMERA_ROTATE_X,
  CAMERA_ROTATE_Y,
  CAMERA_ZOOM,
  MOUSE_DRAG,
  MOUSE_SCROLL,
} from '../model/Event'
import { Config, Const, Store } from '../model/index'
import { emit, listen, on } from '../lib/Emit3r'

const { BUTTON, KEY } = Const
const { dragSpeed }   = Config.controls

@listen
export default class CameraControl {

  static update() {
    const { keyMoveSpeed } = Config.controls

    if (Store.keys[KEY.LEFT]
        + Store.keys[KEY.RIGHT]
        + Store.keys[KEY.UP]
        + Store.keys[KEY.DOWN] === 0) {
      return
    }
    const dx = (-Store.keys[KEY.LEFT] + Store.keys[KEY.RIGHT]) * keyMoveSpeed
    const dy = (-Store.keys[KEY.UP] + Store.keys[KEY.DOWN]) * keyMoveSpeed

    emit(CAMERA_MOVE, dx, dy)
  }

  @on(MOUSE_DRAG)
  drag(dx: number, dy: number, button: number) {
    if (button === BUTTON.LEFT) {
      emit(CAMERA_MOVE, -dx * dragSpeed, dy * dragSpeed)
    }
    if (button === BUTTON.RIGHT) {
      emit(CAMERA_ROTATE_X, -dy)
      emit(CAMERA_ROTATE_Y, -dx)
    }
  }

  @on(MOUSE_SCROLL)
  zoom(delta: number) {
    emit(CAMERA_ZOOM, delta)
  }
}
