// @flow
import { Const, Store } from '../model'
import {
  HUD_CLICK,
  MOUSE_CLICK,
  MOUSE_DRAG,
  MOUSE_HOVER,
  MOUSE_SET_CURSOR,
  MOUSE_SCROLL,
  RESOURCES_LOADED
} from '../model/Event'
import { emit, listen, on } from '../lib/Emit3r'

const { CLICK_MS_MAX } = Const.TIME

const INIT_DRAG = { x: 0, y: 0, button: -1, active: false }

const cursorNode: any = document.getElementById('cursor')

let mouseX: number     = 0
let mouseY: number     = 0
let width3D: number    = 0
let height3D: number   = 0
let cursorName: string = 'default'
let mouseUpTime: number
let mouseDownTime: number

let pressedButton: number = -1
const drag: Object        = INIT_DRAG

@listen
export default class Mouse {

  @on(RESOURCES_LOADED)
  init() {
    width3D  = Store.container.clientWidth
    height3D = Store.container.clientHeight
    this.setCursor(Const.COLOR.CURSOR_2D.DEFAULT)
  }

  @on(window, 'wheel')
  zoom({ deltaY, clientY }: Object) {
    if (clientY < height3D) {
      emit(MOUSE_SCROLL, deltaY)
    }
  }

  @on(window, 'resize')
  resize() {
    width3D  = Store.container.clientWidth
    height3D = Store.container.clientHeight
  }

  @on(window, 'mousedown')
  press({ button }: Object) {
    drag.x        = mouseX
    drag.y        = mouseY
    pressedButton = button
    mouseDownTime = new Date().getTime()
  }

  @on(window, 'mouseup')
  unPress({ button, clientX, clientY }: Object) {
    mouseUpTime   = new Date().getTime()
    const time    = mouseUpTime - mouseDownTime
    pressedButton = -1

    if (!drag.active || time < CLICK_MS_MAX) {
      if (clientY < height3D) {
        emit(MOUSE_CLICK, mouseX, mouseY, button)
      } else {
        emit(HUD_CLICK, clientX, clientY, button)
      }
    }
    drag.active = false
  }

  @on(window, 'mousemove')
  move({ clientX, clientY }: Object) {
    cursorNode.style.left = `${clientX}px`
    cursorNode.style.top  = `${clientY}px`

    mouseX = (clientX / width3D) * 2 - 1
    mouseY = -(clientY / height3D) * 2 + 1
    if (clientY < height3D) {
      cursorNode.style.display = 'block'
      if (pressedButton !== -1) {
        this.doDrag()
      } else {
        emit(MOUSE_HOVER, mouseX, mouseY)
      }
    } else {
      cursorNode.style.display = 'none'
    }
  }

  @on(MOUSE_SET_CURSOR)
  setCursor(newName: string) {
    if (cursorName !== newName) {
      Store.resources.cursor[cursorName].style.display = 'none'
      Store.resources.cursor[newName].style.display    = 'block'

      cursorName = newName
    }
  }

  doDrag() {
    const { x, y }   = drag
    const dx: number = mouseX - x
    const dy: number = mouseY - y
    drag.x           = mouseX
    drag.y           = mouseY
    drag.active      = true

    emit(MOUSE_DRAG, dx, dy, pressedButton)
  }
}
