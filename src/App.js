import '../assets/css/app.css'
import Detector from './app/helpers/detector'
import Main from './app/Main'

function init() {
  if (!Detector.webgl) {
    Detector.addGetWebGLMessage()
  } else {
    new Main()
  }
}
init()
